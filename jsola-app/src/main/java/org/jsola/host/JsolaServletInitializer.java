package org.jsola.host;

import org.jsola.core.AbstractJsolaServletInitializer;

/**
 * 支持外置tomcat
 * @author huangrun
 */
public class JsolaServletInitializer extends AbstractJsolaServletInitializer {

    @Override
    protected Class<?> getAppClass() {
        return JsolaApplication.class;
    }
}
