package org.jsola.host.core;

import org.apache.ibatis.session.SqlSessionFactory;
import org.jsola.autoconfigure.orm.mybatis.MybatisConfigurationSupport;
import org.mybatis.spring.boot.autoconfigure.MybatisAutoConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import tk.mybatis.spring.annotation.MapperScan;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import static org.jsola.host.constant.JsolaConstants.TX;
import static org.jsola.host.constant.JsolaConstants.SQL_SESSION_FACTORY_BEAN_NAME;
import javax.sql.DataSource;

/**
 * @author huangrun
 */
@Configuration
@EnableConfigurationProperties(JsolaProperties.class)
@MapperScan(basePackages = "org.jsola.host.dao", sqlSessionFactoryRef = SQL_SESSION_FACTORY_BEAN_NAME)
@AutoConfigureBefore(MybatisAutoConfiguration.class)
public class JsolaAutoConfiguration {

    @Bean(name = SQL_SESSION_FACTORY_BEAN_NAME)
    @ConditionalOnMissingBean(name = SQL_SESSION_FACTORY_BEAN_NAME)
    public SqlSessionFactory jsolaSqlSessionFactory(DataSource datasource, MybatisConfigurationSupport mybatisConfigurationSupport) throws Exception {
        return mybatisConfigurationSupport.build(datasource);
    }

    @Bean(name = TX)
    @ConditionalOnMissingBean(name = TX)
    public DataSourceTransactionManager jsolaTx(DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }


}
