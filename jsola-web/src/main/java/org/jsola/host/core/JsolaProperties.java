package org.jsola.host.core;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 应用的配置信息
 * @author huangrun
 */
@ConfigurationProperties(value = "jsola")
@Data
public class JsolaProperties {


}
