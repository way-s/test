package org.jsola.host.service;

import org.jsola.core.Page;
import org.jsola.core.service.IService;
import org.jsola.host.dao.IStudentDAO;
import org.jsola.host.dto.StudentAddDTO;
import org.jsola.host.dto.StudentUpdateDTO;
import org.jsola.host.entity.StudentDO;
import org.jsola.host.query.StudentQuery;
import org.jsola.host.vo.StudentListVO;
import org.jsola.host.vo.StudentVO;
import org.jsola.user.core.TokenUser;

import java.util.List;


/**
 * 学生表
 *
 * @author huangrun
 */
public interface IStudentService extends IService<IStudentDAO, StudentDO> {

    /**
     * 新增学生表
     * @param studentAddDTO 新增学生表DTO
     * @param tokenUser 当前用户
     * @return 学生表详情信息
     */
    StudentVO save(StudentAddDTO studentAddDTO, TokenUser tokenUser);

    /**
     * 修改学生表
     * @param studentUpdateDTO 修改学生表DTO
     * @param tokenUser 当前用户
     * @return 更新数量
     */
    int update(StudentUpdateDTO studentUpdateDTO, TokenUser tokenUser);


    /**
     * 批量删除学生表，物理删除，更新is_valid字段，从回收站删除
     * @param tokenUser 当前用户
     * @param studentIds 学生表id
     * @return 删除数量
     */
    int deleteByIds(TokenUser tokenUser, Long...studentIds);

    /**
     * 根据学生表id查找
     * @param studentId 学生表id
     * @param siteId 所属站点id
     * @return 学生表详情信息
     */
    StudentVO selectById(Long studentId, String siteId);

    /**
     * 查询学生表
     * @param studentQuery 查询条件
     * @param siteId 所属站点id
     * @return 学生表列表信息
     */
    List<StudentListVO> select(StudentQuery studentQuery, String siteId);

    /**
    * 查询学生表记录数
    * @param studentQuery 查询条件
    * @param siteId 所属站点id
    * @return 学生表记录数
    */
    int selectCount(StudentQuery studentQuery, String siteId);

    /**
     * 分页查询学生表
     * @param studentQuery 查询条件
     * @param siteId 所属站点id
     * @return 学生表列表信息
     */
    Page<StudentListVO> selectPage(StudentQuery studentQuery, String siteId);


    /**
     * 根据学生表id查找
     * @param studentId 学生表id
     * @param siteId 所属站点id
     * @return 学生表
     */
    StudentDO selectDOById(Long studentId, String siteId);

    /**
     * 查询学生表
     * @param studentQuery 查询条件
     * @param siteId 所属站点id
     * @return 学生表列表
     */
    List<StudentDO> selectDO(StudentQuery studentQuery, String siteId);
}


