package org.jsola.host.service.impl;

import org.jsola.core.Page;
import org.jsola.host.common.ExampleKit;
import org.jsola.host.constant.JsolaConstants;
import org.jsola.host.dao.IStudentDAO;
import org.jsola.host.dto.StudentAddDTO;
import org.jsola.host.dto.StudentUpdateDTO;
import org.jsola.host.entity.StudentDO;
import org.jsola.host.query.StudentQuery;
import org.jsola.host.service.IStudentService;
import org.jsola.host.vo.StudentListVO;
import org.jsola.host.vo.StudentVO;
import org.jsola.user.core.TokenUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 学生表
 *
 * @author huangrun
 */
@Service("jsolaStudentServiceImpl")
public class StudentServiceImpl implements IStudentService {

    @Autowired
    private IStudentDAO studentDAO;

    @Override
    @Transactional(value = JsolaConstants.TX, rollbackFor = Exception.class)
    public StudentVO save(StudentAddDTO studentAddDTO, TokenUser tokenUser) {
        // 转DO
        StudentDO studentDO = studentAddDTO.to(StudentDO.class);
        // 保存
        studentDO = save(studentDO, tokenUser.getSiteId(), tokenUser.getUserId());
        return studentDO.to(StudentVO.class);
    }

    @Override
    @Transactional(value = JsolaConstants.TX, rollbackFor = Exception.class)
    public int update(StudentUpdateDTO studentUpdateDTO, TokenUser tokenUser) {
        //转DO
        StudentDO studentDO = studentUpdateDTO.to(StudentDO.class);
        //根据主键更新，只更新非null值
        return updateByIdSelective(studentDO, tokenUser.getSiteId(), tokenUser.getUserId());
    }

    @Override
    @Transactional(value = JsolaConstants.TX, rollbackFor = Exception.class)
    public int deleteByIds(TokenUser tokenUser, Long... studentIds) {
        return deleteByIds(tokenUser.getSiteId(), tokenUser.getUserId(), (Object[]) studentIds);
    }


    @Override
    public StudentVO selectById(Long studentId, String siteId) {
        StudentDO studentDO = selectDOById(studentId, siteId);
        if (studentDO == null) {
            return null;
        }
        return studentDO.to(StudentVO.class);
    }

    @Override
    public List<StudentListVO> select(StudentQuery studentQuery, String siteId) {
        List<StudentDO> studentDOList = selectDO(studentQuery, siteId);
        if(CollectionUtils.isEmpty(studentDOList)) {
            return studentDOList == null ? null : new ArrayList<>();
        }
        return studentDOList.stream()
                .map(studentDO -> studentDO.to(StudentListVO.class))
                .collect(Collectors.toList());
    }

    @Override
    public int selectCount(StudentQuery studentQuery, String siteId) {
        Example example = buildExample(studentQuery,siteId);
        return studentDAO.selectCountByExample(example);
    }

    @Override
    public Page<StudentListVO> selectPage(StudentQuery studentQuery, String siteId) {
        Example example = buildExample(studentQuery,siteId);
        Page<StudentDO> page = studentDAO.selectPageByExample(example,
                studentQuery.getPageNo(),
                studentQuery.getPageSize());

        return page.to(StudentListVO.class);
    }

    @Override
    public StudentDO selectDOById(Long studentId, String siteId) {
        return listById(studentId, siteId);
    }

    @Override
    public List<StudentDO> selectDO(StudentQuery studentQuery, String siteId) {
        Example example = buildExample(studentQuery,siteId);
        return studentDAO.selectByExample(example);
    }


    /**
     * 根据查询参数，构建example

     * @param studentQuery 查询参数
     * @param siteId 所属站点id
     * @return example
     */
    private Example buildExample(StudentQuery studentQuery, String siteId) {
        Example example = new Example(StudentDO.class);
        example.and()
                .andEqualTo("siteId", siteId);
        // 排序
        ExampleKit.setExampleOrder(example,studentQuery.getOrders());
        return example;
    }
}




