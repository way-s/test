package org.jsola.host.service;


/**
 * @author huangrun
 */
public interface IJsolaPermissionService {

    /**
     * 验证用户是否拥有某个对象的权限
     * @param userId 用户id
     * @param siteId 站点id
     * @param subjectId 业务ID
     * @param subjectType 业务类型
     * @return 拥有权限：true，否则false
     */
    boolean hasRight(String userId, String siteId, String subjectId, String subjectType);

    /**
     * 验证用户是否拥有指定权限
     * @param userId 用户id
     * @param siteId 站点id
     * @param permissionString 权限字符串 如site:product.manager:save:1
     * @return 拥有权限：true，否则false
     */
    boolean hasRight(String userId, String siteId, String permissionString);

    /**
     * 验证用户是否拥有所有指定权限
     * @param userId 用户id
     * @param siteId 站点id
     * @param permissionString 权限字符串 如site:product.manager:save:1
     * @return 拥有权限：true，否则false
     */
    boolean hasAllRight(String userId, String siteId, String... permissionString);

    /**
     * 验证用户是否拥有以下指定权限的某一个权限
     * @param userId 用户id
     * @param siteId 站点id
     * @param permissionString 权限字符串 如site:product.manager:save:1
     * @return 拥有权限：true，否则false
     */
    boolean hasAnyRight(String userId, String siteId, String... permissionString);

    /**
     * 验证用户是否拥有某个实例指定权限
     * @param userId 用户id
     * @param siteId 站点id
     * @param subjectId 业务ID
     * @param permissionString 权限字符串 如site:product.manager:save 不能添加实例
     * @return 拥有权限：true，否则false
     */
    boolean hasObjRight(String userId, String siteId, String subjectId, String permissionString);


    /**
     * 验证用户是否拥有某个实例所有指定权限
     * @param userId 用户id
     * @param siteId 站点id
     * @param subjectId 业务ID
     * @param permissionString 权限字符串 如site:product.manager:save 不能添加实例
     * @return 拥有权限：true，否则false
     */
    boolean hasObjAllRight(String userId, String siteId, String subjectId, String... permissionString);

    /**
     * 验证用户是否拥有某个实例以下指定权限的某一个权限
     * @param userId 用户id
     * @param siteId 站点id
     * @param subjectId 业务ID
     * @param permissionString 权限字符串 如site:product.manager:save 不能添加实例
     * @return 拥有权限：true，否则false
     */
    boolean hasObjAnyRight(String userId, String siteId, String subjectId, String... permissionString);

    /**
     * 验证用户是否是该对象的管理员
     * @param userId 用户id
     * @param siteId 站点id
     * @param subjectId 业务ID
     * @param subjectType 业务类型
     * @return 是管理员：true，否则false
     */
    boolean isAdmin(String userId, String siteId, String subjectId, String subjectType);

}


