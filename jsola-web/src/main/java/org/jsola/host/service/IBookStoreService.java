package org.jsola.host.service;

import org.jsola.core.Page;
import org.jsola.core.service.IService;
import org.jsola.host.dao.IBookStoreDAO;
import org.jsola.host.dto.BookStoreAddDTO;
import org.jsola.host.dto.BookStoreUpdateDTO;
import org.jsola.host.entity.BookStoreDO;
import org.jsola.host.query.BookStoreQuery;
import org.jsola.host.vo.BookStoreListVO;
import org.jsola.host.vo.BookStoreVO;
import org.jsola.user.core.TokenUser;

import java.util.List;


/**
 * 书籍信息表
 *
 * @author huangrun
 */
public interface IBookStoreService extends IService<IBookStoreDAO, BookStoreDO> {

    /**
     * 新增书籍信息表
     * @param bookStoreAddDTO 新增书籍信息表DTO
     * @param tokenUser 当前用户
     * @return 书籍信息表详情信息
     */
    BookStoreVO save(BookStoreAddDTO bookStoreAddDTO, TokenUser tokenUser);

    /**
     * 修改书籍信息表
     * @param bookStoreUpdateDTO 修改书籍信息表DTO
     * @param tokenUser 当前用户
     * @return 更新数量
     */
    int update(BookStoreUpdateDTO bookStoreUpdateDTO, TokenUser tokenUser);


    /**
     * 批量删除书籍信息表，物理删除，更新is_valid字段，从回收站删除
     * @param tokenUser 当前用户
     * @param bookStoreIds 书籍信息表id
     * @return 删除数量
     */
    int deleteByIds(TokenUser tokenUser, Long...bookStoreIds);

    /**
     * 根据书籍信息表id查找
     * @param bookStoreId 书籍信息表id
     * @param siteId 所属站点id
     * @return 书籍信息表详情信息
     */
    BookStoreVO selectById(Long bookStoreId, String siteId);

    /**
     * 查询书籍信息表
     * @param bookStoreQuery 查询条件
     * @param siteId 所属站点id
     * @return 书籍信息表列表信息
     */
    List<BookStoreListVO> select(BookStoreQuery bookStoreQuery, String siteId);

    /**
    * 查询书籍信息表记录数
    * @param bookStoreQuery 查询条件
    * @param siteId 所属站点id
    * @return 书籍信息表记录数
    */
    int selectCount(BookStoreQuery bookStoreQuery, String siteId);

    /**
     * 分页查询书籍信息表
     * @param bookStoreQuery 查询条件
     * @param siteId 所属站点id
     * @return 书籍信息表列表信息
     */
    Page<BookStoreListVO> selectPage(BookStoreQuery bookStoreQuery, String siteId);


    /**
     * 根据书籍信息表id查找
     * @param bookStoreId 书籍信息表id
     * @param siteId 所属站点id
     * @return 书籍信息表
     */
    BookStoreDO selectDOById(Long bookStoreId, String siteId);

    /**
     * 查询书籍信息表
     * @param bookStoreQuery 查询条件
     * @param siteId 所属站点id
     * @return 书籍信息表列表
     */
    List<BookStoreDO> selectDO(BookStoreQuery bookStoreQuery, String siteId);
}


