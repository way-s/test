package org.jsola.host.service;

import org.jsola.core.Page;
import org.jsola.core.service.IService;
import org.jsola.host.dao.IAdminDAO;
import org.jsola.host.dto.AdminAddDTO;
import org.jsola.host.dto.AdminUpdateDTO;
import org.jsola.host.entity.AdminDO;
import org.jsola.host.query.AdminQuery;
import org.jsola.host.vo.AdminListVO;
import org.jsola.host.vo.AdminVO;
import org.jsola.user.core.TokenUser;

import java.util.List;


/**
 * 管理员
 *
 * @author huangrun
 */
public interface IAdminService extends IService<IAdminDAO, AdminDO> {

    /**
     * 新增管理员
     * @param adminAddDTO 新增管理员DTO
     * @param tokenUser 当前用户
     * @return 管理员详情信息
     */
    AdminVO save(AdminAddDTO adminAddDTO, TokenUser tokenUser);

    /**
     * 修改管理员
     * @param adminUpdateDTO 修改管理员DTO
     * @param tokenUser 当前用户
     * @return 更新数量
     */
    int update(AdminUpdateDTO adminUpdateDTO, TokenUser tokenUser);


    /**
     * 批量删除管理员，物理删除，更新is_valid字段，从回收站删除
     * @param tokenUser 当前用户
     * @param adminIds 管理员id
     * @return 删除数量
     */
    int deleteByIds(TokenUser tokenUser, Long...adminIds);

    /**
     * 根据管理员id查找
     * @param adminId 管理员id
     * @param siteId 所属站点id
     * @return 管理员详情信息
     */
    AdminVO selectById(Long adminId, String siteId);

    /**
     * 查询管理员
     * @param adminQuery 查询条件
     * @param siteId 所属站点id
     * @return 管理员列表信息
     */
    List<AdminListVO> select(AdminQuery adminQuery, String siteId);

    /**
    * 查询管理员记录数
    * @param adminQuery 查询条件
    * @param siteId 所属站点id
    * @return 管理员记录数
    */
    int selectCount(AdminQuery adminQuery, String siteId);

    /**
     * 分页查询管理员
     * @param adminQuery 查询条件
     * @param siteId 所属站点id
     * @return 管理员列表信息
     */
    Page<AdminListVO> selectPage(AdminQuery adminQuery, String siteId);


    /**
     * 根据管理员id查找
     * @param adminId 管理员id
     * @param siteId 所属站点id
     * @return 管理员
     */
    AdminDO selectDOById(Long adminId, String siteId);

    /**
     * 查询管理员
     * @param adminQuery 查询条件
     * @param siteId 所属站点id
     * @return 管理员列表
     */
    List<AdminDO> selectDO(AdminQuery adminQuery, String siteId);
}


