package org.jsola.host.service.impl;

import org.jsola.core.Page;
import org.jsola.host.common.ExampleKit;
import org.jsola.host.constant.JsolaConstants;
import org.jsola.host.dao.IAdminDAO;
import org.jsola.host.dto.AdminAddDTO;
import org.jsola.host.dto.AdminUpdateDTO;
import org.jsola.host.entity.AdminDO;
import org.jsola.host.query.AdminQuery;
import org.jsola.host.service.IAdminService;
import org.jsola.host.vo.AdminListVO;
import org.jsola.host.vo.AdminVO;
import org.jsola.user.core.TokenUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 管理员
 *
 * @author huangrun
 */
@Service("jsolaAdminServiceImpl")
public class AdminServiceImpl implements IAdminService {

    @Autowired
    private IAdminDAO adminDAO;

    @Override
    @Transactional(value = JsolaConstants.TX, rollbackFor = Exception.class)
    public AdminVO save(AdminAddDTO adminAddDTO, TokenUser tokenUser) {
        // 转DO
        AdminDO adminDO = adminAddDTO.to(AdminDO.class);
        // 保存
        adminDO = save(adminDO, tokenUser.getSiteId(), tokenUser.getUserId());
        return adminDO.to(AdminVO.class);
    }

    @Override
    @Transactional(value = JsolaConstants.TX, rollbackFor = Exception.class)
    public int update(AdminUpdateDTO adminUpdateDTO, TokenUser tokenUser) {
        //转DO
        AdminDO adminDO = adminUpdateDTO.to(AdminDO.class);
        //根据主键更新，只更新非null值
        return updateByIdSelective(adminDO, tokenUser.getSiteId(), tokenUser.getUserId());
    }

    @Override
    @Transactional(value = JsolaConstants.TX, rollbackFor = Exception.class)
    public int deleteByIds(TokenUser tokenUser, Long... adminIds) {
        return deleteByIds(tokenUser.getSiteId(), tokenUser.getUserId(), (Object[]) adminIds);
    }


    @Override
    public AdminVO selectById(Long adminId, String siteId) {
        AdminDO adminDO = selectDOById(adminId, siteId);
        if (adminDO == null) {
            return null;
        }
        return adminDO.to(AdminVO.class);
    }

    @Override
    public List<AdminListVO> select(AdminQuery adminQuery, String siteId) {
        List<AdminDO> adminDOList = selectDO(adminQuery, siteId);
        if(CollectionUtils.isEmpty(adminDOList)) {
            return adminDOList == null ? null : new ArrayList<>();
        }
        return adminDOList.stream()
                .map(adminDO -> adminDO.to(AdminListVO.class))
                .collect(Collectors.toList());
    }

    @Override
    public int selectCount(AdminQuery adminQuery, String siteId) {
        Example example = buildExample(adminQuery,siteId);
        return adminDAO.selectCountByExample(example);
    }

    @Override
    public Page<AdminListVO> selectPage(AdminQuery adminQuery, String siteId) {
        Example example = buildExample(adminQuery,siteId);
        Page<AdminDO> page = adminDAO.selectPageByExample(example,
                adminQuery.getPageNo(),
                adminQuery.getPageSize());

        return page.to(AdminListVO.class);
    }

    @Override
    public AdminDO selectDOById(Long adminId, String siteId) {
        return listById(adminId, siteId);
    }

    @Override
    public List<AdminDO> selectDO(AdminQuery adminQuery, String siteId) {
        Example example = buildExample(adminQuery,siteId);
        return adminDAO.selectByExample(example);
    }


    /**
     * 根据查询参数，构建example

     * @param adminQuery 查询参数
     * @param siteId 所属站点id
     * @return example
     */
    private Example buildExample(AdminQuery adminQuery, String siteId) {
        Example example = new Example(AdminDO.class);
        example.and()
                .andEqualTo("siteId", siteId);
        // 排序
        ExampleKit.setExampleOrder(example,adminQuery.getOrders());
        return example;
    }
}




