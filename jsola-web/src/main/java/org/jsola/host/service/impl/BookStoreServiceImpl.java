package org.jsola.host.service.impl;

import org.jsola.core.Page;
import org.jsola.host.common.ExampleKit;
import org.jsola.host.constant.JsolaConstants;
import org.jsola.host.dao.IBookStoreDAO;
import org.jsola.host.dto.BookStoreAddDTO;
import org.jsola.host.dto.BookStoreUpdateDTO;
import org.jsola.host.entity.BookStoreDO;
import org.jsola.host.query.BookStoreQuery;
import org.jsola.host.service.IBookStoreService;
import org.jsola.host.vo.BookStoreListVO;
import org.jsola.host.vo.BookStoreVO;
import org.jsola.user.core.TokenUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 书籍信息表
 *
 * @author huangrun
 */
@Service("jsolaBookStoreServiceImpl")
public class BookStoreServiceImpl implements IBookStoreService {

    @Autowired
    private IBookStoreDAO bookStoreDAO;

    @Override
    @Transactional(value = JsolaConstants.TX, rollbackFor = Exception.class)
    public BookStoreVO save(BookStoreAddDTO bookStoreAddDTO, TokenUser tokenUser) {
        // 转DO
        BookStoreDO bookStoreDO = bookStoreAddDTO.to(BookStoreDO.class);
        // 保存
        bookStoreDO = save(bookStoreDO, tokenUser.getSiteId(), tokenUser.getUserId());
        return bookStoreDO.to(BookStoreVO.class);
    }

    @Override
    @Transactional(value = JsolaConstants.TX, rollbackFor = Exception.class)
    public int update(BookStoreUpdateDTO bookStoreUpdateDTO, TokenUser tokenUser) {
        //转DO
        BookStoreDO bookStoreDO = bookStoreUpdateDTO.to(BookStoreDO.class);
        //根据主键更新，只更新非null值
        return updateByIdSelective(bookStoreDO, tokenUser.getSiteId(), tokenUser.getUserId());
    }

    @Override
    @Transactional(value = JsolaConstants.TX, rollbackFor = Exception.class)
    public int deleteByIds(TokenUser tokenUser, Long... bookStoreIds) {
        return deleteByIds(tokenUser.getSiteId(), tokenUser.getUserId(), (Object[]) bookStoreIds);
    }


    @Override
    public BookStoreVO selectById(Long bookStoreId, String siteId) {
        BookStoreDO bookStoreDO = selectDOById(bookStoreId, siteId);
        if (bookStoreDO == null) {
            return null;
        }
        return bookStoreDO.to(BookStoreVO.class);
    }

    @Override
    public List<BookStoreListVO> select(BookStoreQuery bookStoreQuery, String siteId) {
        List<BookStoreDO> bookStoreDOList = selectDO(bookStoreQuery, siteId);
        if(CollectionUtils.isEmpty(bookStoreDOList)) {
            return bookStoreDOList == null ? null : new ArrayList<>();
        }
        return bookStoreDOList.stream()
                .map(bookStoreDO -> bookStoreDO.to(BookStoreListVO.class))
                .collect(Collectors.toList());
    }

    @Override
    public int selectCount(BookStoreQuery bookStoreQuery, String siteId) {
        Example example = buildExample(bookStoreQuery,siteId);
        return bookStoreDAO.selectCountByExample(example);
    }

    @Override
    public Page<BookStoreListVO> selectPage(BookStoreQuery bookStoreQuery, String siteId) {
        Example example = buildExample(bookStoreQuery,siteId);
        Page<BookStoreDO> page = bookStoreDAO.selectPageByExample(example,
                bookStoreQuery.getPageNo(),
                bookStoreQuery.getPageSize());

        return page.to(BookStoreListVO.class);
    }

    @Override
    public BookStoreDO selectDOById(Long bookStoreId, String siteId) {
        return listById(bookStoreId, siteId);
    }

    @Override
    public List<BookStoreDO> selectDO(BookStoreQuery bookStoreQuery, String siteId) {
        Example example = buildExample(bookStoreQuery,siteId);
        return bookStoreDAO.selectByExample(example);
    }


    /**
     * 根据查询参数，构建example

     * @param bookStoreQuery 查询参数
     * @param siteId 所属站点id
     * @return example
     */
    private Example buildExample(BookStoreQuery bookStoreQuery, String siteId) {
        Example example = new Example(BookStoreDO.class);
        example.and()
                .andEqualTo("siteId", siteId);
        // 排序
        ExampleKit.setExampleOrder(example,bookStoreQuery.getOrders());
        return example;
    }
}




