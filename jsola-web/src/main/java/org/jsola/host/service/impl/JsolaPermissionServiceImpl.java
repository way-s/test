package org.jsola.host.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.jsola.host.service.IJsolaPermissionService;
import org.springframework.stereotype.Service;

/**
 * @author huangrun
 */
@Service
@Slf4j
public class JsolaPermissionServiceImpl implements IJsolaPermissionService {

    @Override
    public boolean hasRight(String userId, String siteId, String subjectId, String subjectType) {
        return false;
    }

    @Override
    public boolean hasRight(String userId, String siteId, String permissionString) {
        return false;
    }

    @Override
    public boolean hasAllRight(String userId, String siteId, String... permissionString) {
        return false;
    }

    @Override
    public boolean hasAnyRight(String userId, String siteId, String... permissionString) {
        return false;
    }

    @Override
    public boolean hasObjRight(String userId, String siteId, String subjectId, String permissionString) {
        return false;
    }

    @Override
    public boolean hasObjAllRight(String userId, String siteId, String subjectId, String... permissionString) {
        return false;
    }

    @Override
    public boolean hasObjAnyRight(String userId, String siteId, String subjectId, String... permissionString) {
        return false;
    }

    @Override
    public boolean isAdmin(String userId, String siteId, String subjectId, String subjectType) {
        return false;
    }

}
