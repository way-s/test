package org.jsola.host.dao;

import org.jsola.host.entity.BookStoreDO;
import org.jsola.orm.mapper.IJsolaMapper;
import org.springframework.stereotype.Repository;


/**
 * 书籍信息表
 *
 * @author huangrun
 */
@Repository("jsolaBookStoreDAO")
public interface IBookStoreDAO extends IJsolaMapper<BookStoreDO> {

}

