package org.jsola.host.dao;

import org.jsola.host.entity.StudentDO;
import org.jsola.orm.mapper.IJsolaMapper;
import org.springframework.stereotype.Repository;


/**
 * 学生表
 *
 * @author huangrun
 */
@Repository("jsolaStudentDAO")
public interface IStudentDAO extends IJsolaMapper<StudentDO> {

}

