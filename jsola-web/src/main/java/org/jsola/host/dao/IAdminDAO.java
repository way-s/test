package org.jsola.host.dao;

import org.jsola.host.entity.AdminDO;
import org.jsola.orm.mapper.IJsolaMapper;
import org.springframework.stereotype.Repository;


/**
 * 管理员
 *
 * @author huangrun
 */
@Repository("jsolaAdminDAO")
public interface IAdminDAO extends IJsolaMapper<AdminDO> {

}

