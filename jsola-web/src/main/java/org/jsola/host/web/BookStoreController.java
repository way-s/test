package org.jsola.host.web;

import org.jsola.core.Page;
import org.jsola.core.Result;
import org.jsola.exception.ParamException;
import org.jsola.host.constant.JsolaConstants;
import org.jsola.host.dto.BookStoreAddDTO;
import org.jsola.host.dto.BookStoreUpdateDTO;
import org.jsola.host.query.BookStoreQuery;
import org.jsola.host.service.IBookStoreService;
import org.jsola.host.vo.BookStoreListVO;
import org.jsola.host.vo.BookStoreVO;
import org.jsola.user.core.TokenUser;
import org.jsola.user.core.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import javax.validation.Valid;


/**
 * 书籍信息表
 *
 * @author huangrun
 */
@RestController("jsolaBookStoreController")
@RequestMapping(JsolaConstants.API_PATH + "/v1/bookStores")
public class BookStoreController implements IBookStoreControllerAPI {

    @Autowired
    private IBookStoreService bookStoreService;

    @Override
    @PostMapping
    public Result<BookStoreVO> save(@Valid @RequestBody BookStoreAddDTO bookStoreAddDTO) {
        TokenUser tokenUser = UserContext.getUser();
        // todo 新增书籍信息表权限校验

        return Result.success(
                bookStoreService.save(bookStoreAddDTO, tokenUser)
        );
    }


    @Override
    @PutMapping
    public Result<Integer> update(@Valid @RequestBody BookStoreUpdateDTO bookStoreUpdateDTO) {
        TokenUser tokenUser = UserContext.getUser();
        // todo 修改书籍信息表权限校验

        int count = bookStoreService.update(bookStoreUpdateDTO, tokenUser);
        if (count == 0) {
            throw new ParamException("更新失败，请刷新页面后重试");
        }
        return Result.success(count);
    }



    @Override
    @DeleteMapping(value = "/{bookStoreId}")
    public Result<Integer> delete(@PathVariable Long bookStoreId) {
        TokenUser tokenUser = UserContext.getUser();
        // todo 彻底删除书籍信息表权限校验

        int count = bookStoreService.deleteByIds(tokenUser, bookStoreId);
        if (count == 0) {
            throw new ParamException("删除失败，请刷新页面后重试");
        }
        return Result.success(count);
    }

    @Override
    @DeleteMapping
    public Result<Integer> deleteBatch(@RequestBody Long[] bookStoreIds) {
        TokenUser tokenUser = UserContext.getUser();
        // todo 批量彻底删除书籍信息表权限校验

        int count = bookStoreService.deleteByIds(tokenUser, bookStoreIds);
        if (count == 0) {
            throw new ParamException("删除失败，请刷新页面后重试");
        }
        return Result.success(count);
    }

    @Override
    @GetMapping(value = "/{bookStoreId}")
    public Result<BookStoreVO> findById(@PathVariable Long bookStoreId) {
        TokenUser tokenUser = UserContext.getUser();
        // todo 查看书籍信息表权限校验

        return Result.success(
            bookStoreService.selectById(bookStoreId, tokenUser.getSiteId())
        );
    }

    @Override
    @GetMapping(value = "/listAll")
    public Result<List<BookStoreListVO>> listAll(@Valid BookStoreQuery bookStoreQuery) {
        TokenUser tokenUser = UserContext.getUser();
        // todo 查看书籍信息表权限校验

        return Result.success(
            bookStoreService.select(bookStoreQuery, tokenUser.getSiteId())
        );
    }


    @Override
    @GetMapping
    public Result<Page<BookStoreListVO>> page(@Valid BookStoreQuery bookStoreQuery) {
        TokenUser tokenUser = UserContext.getUser();
        // todo 查看项目权限校验

        return Result.success(
            bookStoreService.selectPage(bookStoreQuery, tokenUser.getSiteId())
        );
    }


}

