package org.jsola.host.web;

import org.jsola.core.Page;
import org.jsola.core.Result;
import org.jsola.exception.ParamException;
import org.jsola.host.constant.JsolaConstants;
import org.jsola.host.dto.StudentAddDTO;
import org.jsola.host.dto.StudentUpdateDTO;
import org.jsola.host.query.StudentQuery;
import org.jsola.host.service.IStudentService;
import org.jsola.host.vo.StudentListVO;
import org.jsola.host.vo.StudentVO;
import org.jsola.user.core.TokenUser;
import org.jsola.user.core.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import javax.validation.Valid;


/**
 * 学生表
 *
 * @author huangrun
 */
@RestController("jsolaStudentController")
@RequestMapping(JsolaConstants.API_PATH + "/v1/students")
public class StudentController implements IStudentControllerAPI {

    @Autowired
    private IStudentService studentService;

    @Override
    @PostMapping
    public Result<StudentVO> save(@Valid @RequestBody StudentAddDTO studentAddDTO) {
        TokenUser tokenUser = UserContext.getUser();
        // todo 新增学生表权限校验

        return Result.success(
                studentService.save(studentAddDTO, tokenUser)
        );
    }


    @Override
    @PutMapping
    public Result<Integer> update(@Valid @RequestBody StudentUpdateDTO studentUpdateDTO) {
        TokenUser tokenUser = UserContext.getUser();
        // todo 修改学生表权限校验

        int count = studentService.update(studentUpdateDTO, tokenUser);
        if (count == 0) {
            throw new ParamException("更新失败，请刷新页面后重试");
        }
        return Result.success(count);
    }



    @Override
    @DeleteMapping(value = "/{studentId}")
    public Result<Integer> delete(@PathVariable Long studentId) {
        TokenUser tokenUser = UserContext.getUser();
        // todo 彻底删除学生表权限校验

        int count = studentService.deleteByIds(tokenUser, studentId);
        if (count == 0) {
            throw new ParamException("删除失败，请刷新页面后重试");
        }
        return Result.success(count);
    }

    @Override
    @DeleteMapping
    public Result<Integer> deleteBatch(@RequestBody Long[] studentIds) {
        TokenUser tokenUser = UserContext.getUser();
        // todo 批量彻底删除学生表权限校验

        int count = studentService.deleteByIds(tokenUser, studentIds);
        if (count == 0) {
            throw new ParamException("删除失败，请刷新页面后重试");
        }
        return Result.success(count);
    }

    @Override
    @GetMapping(value = "/{studentId}")
    public Result<StudentVO> findById(@PathVariable Long studentId) {
        TokenUser tokenUser = UserContext.getUser();
        // todo 查看学生表权限校验

        return Result.success(
            studentService.selectById(studentId, tokenUser.getSiteId())
        );
    }

    @Override
    @GetMapping(value = "/listAll")
    public Result<List<StudentListVO>> listAll(@Valid StudentQuery studentQuery) {
        TokenUser tokenUser = UserContext.getUser();
        // todo 查看学生表权限校验

        return Result.success(
            studentService.select(studentQuery, tokenUser.getSiteId())
        );
    }


    @Override
    @GetMapping
    public Result<Page<StudentListVO>> page(@Valid StudentQuery studentQuery) {
        TokenUser tokenUser = UserContext.getUser();
        // todo 查看项目权限校验

        return Result.success(
            studentService.selectPage(studentQuery, tokenUser.getSiteId())
        );
    }


}

