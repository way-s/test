package org.jsola.host.web;

import org.jsola.core.Page;
import org.jsola.core.Result;
import org.jsola.exception.ParamException;
import org.jsola.host.constant.JsolaConstants;
import org.jsola.host.dto.AdminAddDTO;
import org.jsola.host.dto.AdminUpdateDTO;
import org.jsola.host.query.AdminQuery;
import org.jsola.host.service.IAdminService;
import org.jsola.host.vo.AdminListVO;
import org.jsola.host.vo.AdminVO;
import org.jsola.user.core.TokenUser;
import org.jsola.user.core.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import javax.validation.Valid;


/**
 * 管理员
 *
 * @author huangrun
 */
@RestController("jsolaAdminController")
@RequestMapping(JsolaConstants.API_PATH + "/v1/admins")
public class AdminController implements IAdminControllerAPI {

    @Autowired
    private IAdminService adminService;

    @Override
    @PostMapping
    public Result<AdminVO> save(@Valid @RequestBody AdminAddDTO adminAddDTO) {
        TokenUser tokenUser = UserContext.getUser();
        // todo 新增管理员权限校验

        return Result.success(
                adminService.save(adminAddDTO, tokenUser)
        );
    }


    @Override
    @PutMapping
    public Result<Integer> update(@Valid @RequestBody AdminUpdateDTO adminUpdateDTO) {
        TokenUser tokenUser = UserContext.getUser();
        // todo 修改管理员权限校验

        int count = adminService.update(adminUpdateDTO, tokenUser);
        if (count == 0) {
            throw new ParamException("更新失败，请刷新页面后重试");
        }
        return Result.success(count);
    }



    @Override
    @DeleteMapping(value = "/{adminId}")
    public Result<Integer> delete(@PathVariable Long adminId) {
        TokenUser tokenUser = UserContext.getUser();
        // todo 彻底删除管理员权限校验

        int count = adminService.deleteByIds(tokenUser, adminId);
        if (count == 0) {
            throw new ParamException("删除失败，请刷新页面后重试");
        }
        return Result.success(count);
    }

    @Override
    @DeleteMapping
    public Result<Integer> deleteBatch(@RequestBody Long[] adminIds) {
        TokenUser tokenUser = UserContext.getUser();
        // todo 批量彻底删除管理员权限校验

        int count = adminService.deleteByIds(tokenUser, adminIds);
        if (count == 0) {
            throw new ParamException("删除失败，请刷新页面后重试");
        }
        return Result.success(count);
    }

    @Override
    @GetMapping(value = "/{adminId}")
    public Result<AdminVO> findById(@PathVariable Long adminId) {
        TokenUser tokenUser = UserContext.getUser();
        // todo 查看管理员权限校验

        return Result.success(
            adminService.selectById(adminId, tokenUser.getSiteId())
        );
    }

    @Override
    @GetMapping(value = "/listAll")
    public Result<List<AdminListVO>> listAll(@Valid AdminQuery adminQuery) {
        TokenUser tokenUser = UserContext.getUser();
        // todo 查看管理员权限校验

        return Result.success(
            adminService.select(adminQuery, tokenUser.getSiteId())
        );
    }


    @Override
    @GetMapping
    public Result<Page<AdminListVO>> page(@Valid AdminQuery adminQuery) {
        TokenUser tokenUser = UserContext.getUser();
        // todo 查看项目权限校验

        return Result.success(
            adminService.selectPage(adminQuery, tokenUser.getSiteId())
        );
    }


}

