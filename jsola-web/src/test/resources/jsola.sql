    DROP TABLE IF EXISTS `com_admin`;
    CREATE TABLE `com_admin` (
        `id` bigint(11) AUTO_INCREMENT NOT NULL COMMENT '主键ID',
        `name` varchar(12) NOT NULL COMMENT '名字',
        `pwd` varchar(16) NOT NULL COMMENT '密码',
        `tel` varchar(11) NOT NULL COMMENT '编号',
        `site_id` varchar(32) NOT NULL COMMENT '所属站点ID',
        PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='管理员';

    DROP TABLE IF EXISTS `com_book_store`;
    CREATE TABLE `com_book_store` (
        `id` bigint(11) AUTO_INCREMENT NOT NULL COMMENT '主键ID',
        `book_name` varchar(20) NOT NULL COMMENT '书名',
        `author` varchar(20) NOT NULL COMMENT '作者',
        `translator` varchar(20) DEFAULT NULL COMMENT '翻译',
        `bookman` varchar(30) NOT NULL COMMENT '发行社',
        `douban_score` decimal(2,1) DEFAULT NULL COMMENT '发行时间',
        `site_id` varchar(32) NOT NULL COMMENT '所属站点ID',
        PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='书籍信息表';

    DROP TABLE IF EXISTS `com_student`;
    CREATE TABLE `com_student` (
        `id` bigint(11) AUTO_INCREMENT NOT NULL COMMENT '主键ID',
        `time_out` int(1) NOT NULL DEFAULT 0 COMMENT '超时',
        `brr_count` int(11) NOT NULL DEFAULT 10 COMMENT '计数',
        `name` varchar(12) NOT NULL COMMENT '名字',
        `pwd` varchar(16) NOT NULL COMMENT '密码',
        `card` varchar(1) DEFAULT '0' COMMENT '卡 1 有,0 没有',
        `tel` varchar(11) NOT NULL COMMENT '学号',
        `site_id` varchar(32) NOT NULL COMMENT '所属站点ID',
        PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='学生表';

