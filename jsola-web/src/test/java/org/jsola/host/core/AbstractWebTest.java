
package org.jsola.host.core;

import org.jsola.autoconfigure.jwt.SecurityContext;
import org.jsola.user.core.TokenUser;
import org.junit.Before;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.test.web.servlet.setup.ConfigurableMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.test.web.servlet.setup.MockMvcConfigurer;
import org.springframework.test.web.servlet.setup.MockMvcConfigurerAdapter;
import org.springframework.web.context.WebApplicationContext;

import java.beans.PropertyDescriptor;
import java.nio.charset.Charset;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

/**
 * @author huangrun
 */
public abstract class AbstractWebTest extends AbstractTest {

    protected static final MediaType APPLICATION_JSON_UTF8 = new MediaType(
            MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));


    @Autowired
    protected WebApplicationContext webApplicationContext;

    protected MockMvc restMockMvc;

    @Before
    public void setup() {
        this.restMockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .alwaysDo(print())
                .apply(this.getMockMvcConfigurer())
                .alwaysExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .build();
    }

    protected MockMvcConfigurer getMockMvcConfigurer() {
        return new MockMvcConfigurerAdapter() {
            public RequestPostProcessor beforeMockMvcCreated(ConfigurableMockMvcBuilder<?> builder, WebApplicationContext cxt) {
                return mockHttpServletRequest -> {
                    mockHttpServletRequest.addHeader("Accept", MediaType.APPLICATION_JSON_UTF8_VALUE);
                    return mockHttpServletRequest;
                };
            }
        };
    }

    /**
     * 模拟登录
     * @param userId 要登录的用户id
     */
    protected void login(String userId){
        TokenUser tokenUser = new TokenUser();
        tokenUser.setDeviceType("web");
        tokenUser.setLoginType(1);
        tokenUser.setSiteId(SITE_ID);
        tokenUser.setUserId(userId == null ? CURR_USER_ID : userId);
        tokenUser.setUserName("张三");
        tokenUser.setSiteName("test");
        SecurityContext.setTokenData(tokenUser);
    }

    /**
     * 构建get查询请求
     * @param url url
     * @param query 参数
     * @return MockHttpServletRequestBuilder
     * @throws Exception 异常
     */
    protected MockHttpServletRequestBuilder createGetRequestBuilder(String url, Object query) throws Exception {
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(url)
                .contentType(MediaType.APPLICATION_JSON);
        for (PropertyDescriptor property : new BeanWrapperImpl(query).getPropertyDescriptors()) {
            if (property.getWriteMethod() != null) {
                Object value = property.getReadMethod().invoke(query);
                requestBuilder.param(property.getName(),
                        value == null ? null : value.toString());
            }
        }
        return requestBuilder;
    }

}
