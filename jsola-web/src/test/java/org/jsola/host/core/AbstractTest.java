
package org.jsola.host.core;

import lombok.extern.slf4j.Slf4j;
import org.jsola.common.H2Kit;
import org.jsola.orm.dao.IJsolaDao;
import org.jsola.user.core.TokenUser;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import static org.jsola.host.constant.JsolaConstants.TX;

import static org.jsola.host.helper.BaseTestHelper.buildTokenUser;

/**
 * @author huangrun
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Rollback
@Transactional(TX)
@ActiveProfiles({"junitTest"})
@Slf4j
public abstract class AbstractTest {

    @Autowired
    protected IJsolaDao jsolaDao;

    public static final String SITE_ID = "xxxxxxxxxx";

    public static final String MODIFIED_USER_ID = "367C98A49FD111E8B0C27CD30ADFFC52";

    public static final String CURR_USER_ID = "367C98A49FD111E8B0C27CD30ADFFC52";

    @Before
    public void setUp() {
        // h2方式
        jsolaDao.execute("drop all objects;");
        String scriptPath = H2Kit.getH2Script("jsola.sql");
        jsolaDao.execute("runscript from '" + scriptPath + "'");
    }

    /**
     * 获取tokenUser
     * @return tokenUser
     */
    protected TokenUser getTokenUser(){
        return buildTokenUser();
    }


}
