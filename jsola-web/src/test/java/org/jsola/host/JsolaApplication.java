package org.jsola.host;

import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.jsola.autoconfigure.jwt.SecurityContext;
import org.jsola.cache.ICache;
import org.jsola.cache.SimpleMapCache;
import org.jsola.user.core.TokenUser;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.boot.autoconfigure.MybatisProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import static org.jsola.host.constant.JsolaConstants.SQL_SESSION_FACTORY_BEAN_NAME;
import static org.jsola.host.core.AbstractTest.CURR_USER_ID;
import static org.jsola.host.core.AbstractTest.SITE_ID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author huangrun
 */
@SpringBootApplication
@ComponentScan("org.jsola")
public class JsolaApplication {

    @Autowired
    private MybatisProperties mybatisProperties;

    public static void main(String[] args) {
        new SpringApplicationBuilder(JsolaApplication.class)
        .run(args);
    }

    @Bean
    public SqlSessionTemplate sqlSessionTemplate(@Qualifier(SQL_SESSION_FACTORY_BEAN_NAME) SqlSessionFactory sqlSessionFactory) {
        ExecutorType executorType = this.mybatisProperties.getExecutorType();
        if (executorType != null) {
            return new SqlSessionTemplate(sqlSessionFactory, executorType);
        } else {
            return new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    @Bean
    @Profile("junitTest")
    public ICache redisCache() {
        return new SimpleMapCache();
    }

    @Configuration
    class MyWebMvcConfig implements WebMvcConfigurer {
        @Bean
        @Profile("junitTest")
        public MyWebMvcConfig getMyWebMvcConfig() {
            return new MyWebMvcConfig() {
                @Override
                public void addInterceptors(InterceptorRegistry registry) {
                    registry.addInterceptor(new HandlerInterceptor(){
                        @Override
                        public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
                            TokenUser tokenUser = new TokenUser();
                            tokenUser.setDeviceType("web");
                            tokenUser.setLoginType(1);
                            tokenUser.setSiteId(SITE_ID);
                            tokenUser.setUserId(CURR_USER_ID);
                            tokenUser.setUserName("张三");
                            tokenUser.setSiteName("test");
                            SecurityContext.setTokenData(tokenUser);
                            return true;
                        }
                    }).addPathPatterns("/**");
                }
            };
        }
    }


}
