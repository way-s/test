package org.jsola.host.web;

import com.google.common.collect.Lists;
import org.hamcrest.Matchers;
import org.jsola.host.constant.JsolaConstants;
import org.jsola.host.core.AbstractWebTest;
import org.jsola.host.dto.AdminAddDTO;
import org.jsola.host.dto.AdminUpdateDTO;
import org.jsola.host.helper.AdminTestHelper;
import org.jsola.host.query.AdminQuery;
import org.jsola.host.service.IAdminService;
import org.jsola.host.vo.AdminVO;
import org.jsola.json.JsonKit;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 * 管理员
 *
 * @author huangrun
 */
public class AdminControllerTest extends AbstractWebTest {

    @Autowired
    private AdminTestHelper adminTestHelper;
    @Autowired
    private IAdminService adminService;

    @Test
    public void save() throws Exception {
        AdminAddDTO adminAddDTO = adminTestHelper.getAdminAddDTO();
        restMockMvc.perform(
                MockMvcRequestBuilders
                        .post(JsolaConstants.API_PATH + "/v1/admins")
                        .contentType(APPLICATION_JSON_UTF8)
                        .content(JsonKit.toJSONString(adminAddDTO))
        ).andExpect(MockMvcResultMatchers.jsonPath("$.success").value(Matchers.is(true)));
    }

    @Test
    public void update() throws Exception {
        AdminVO adminVO = adminTestHelper.saveAdminExample();
        AdminUpdateDTO adminUpdateDTO = adminTestHelper.getAdminUpdateDTO(adminVO);
        restMockMvc.perform(
                MockMvcRequestBuilders
                        .put(JsolaConstants.API_PATH + "/v1/admins")
                        .contentType(APPLICATION_JSON_UTF8)
                        .content(JsonKit.toJSONString(adminUpdateDTO))
        ).andExpect(MockMvcResultMatchers.jsonPath("$.success").value(Matchers.is(true)));
    }

    @Test
    public void delete() throws Exception {
        AdminVO adminVO = adminTestHelper.saveAdminExample();
        restMockMvc.perform(
                MockMvcRequestBuilders
                        .delete(JsolaConstants.API_PATH + "/v1/admins/{adminId}", adminVO.getId())
                        .contentType(APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.jsonPath("$.success").value(Matchers.is(true)));
    }

    @Test
    public void deleteBatch() throws Exception {
        AdminVO adminVO = adminTestHelper.saveAdminExample();
        restMockMvc.perform(
                MockMvcRequestBuilders
                        .delete(JsolaConstants.API_PATH + "/v1/admins")
                        .content(JsonKit.toJSONString(Lists.newArrayList(adminVO.getId())))
                        .contentType(APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.jsonPath("$.success").value(Matchers.is(true)));
    }
    @Test
    public void findById() throws Exception {
        AdminVO adminVO = adminTestHelper.saveAdminExample();
        restMockMvc.perform(
                MockMvcRequestBuilders
                        .get(JsolaConstants.API_PATH + "/v1/admins/{adminId}", adminVO.getId())
                        .contentType(APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.jsonPath("$.success").value(Matchers.is(true)));
    }

    @Test
    public void listAll() throws Exception {
        AdminVO adminVO = adminTestHelper.saveAdminExample();
        AdminQuery adminQuery = adminTestHelper.getAdminQuery();
        MockHttpServletRequestBuilder requestBuilder = createGetRequestBuilder(JsolaConstants.API_PATH + "/v1/admins/listAll",adminQuery);
        restMockMvc.perform(requestBuilder).andExpect(MockMvcResultMatchers.jsonPath("$.success").value(Matchers.is(true)));
    }

    @Test
    public void page() throws Exception {
        AdminVO adminVO = adminTestHelper.saveAdminExample();
        AdminQuery adminQuery = adminTestHelper.getAdminQuery();
        MockHttpServletRequestBuilder requestBuilder = createGetRequestBuilder(JsolaConstants.API_PATH + "/v1/admins",adminQuery);
        restMockMvc.perform(requestBuilder).andExpect(MockMvcResultMatchers.jsonPath("$.success").value(Matchers.is(true)));
    }



}

