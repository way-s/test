package org.jsola.host.web;

import com.google.common.collect.Lists;
import org.hamcrest.Matchers;
import org.jsola.host.constant.JsolaConstants;
import org.jsola.host.core.AbstractWebTest;
import org.jsola.host.dto.StudentAddDTO;
import org.jsola.host.dto.StudentUpdateDTO;
import org.jsola.host.helper.StudentTestHelper;
import org.jsola.host.query.StudentQuery;
import org.jsola.host.service.IStudentService;
import org.jsola.host.vo.StudentVO;
import org.jsola.json.JsonKit;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 * 学生表
 *
 * @author huangrun
 */
public class StudentControllerTest extends AbstractWebTest {

    @Autowired
    private IStudentService studentService;
    @Autowired
    private StudentTestHelper studentTestHelper;

    @Test
    public void save() throws Exception {
        StudentAddDTO studentAddDTO = studentTestHelper.getStudentAddDTO();
        restMockMvc.perform(
                MockMvcRequestBuilders
                        .post(JsolaConstants.API_PATH + "/v1/students")
                        .contentType(APPLICATION_JSON_UTF8)
                        .content(JsonKit.toJSONString(studentAddDTO))
        ).andExpect(MockMvcResultMatchers.jsonPath("$.success").value(Matchers.is(true)));
    }

    @Test
    public void update() throws Exception {
        StudentVO studentVO = studentTestHelper.saveStudentExample();
        StudentUpdateDTO studentUpdateDTO = studentTestHelper.getStudentUpdateDTO(studentVO);
        restMockMvc.perform(
                MockMvcRequestBuilders
                        .put(JsolaConstants.API_PATH + "/v1/students")
                        .contentType(APPLICATION_JSON_UTF8)
                        .content(JsonKit.toJSONString(studentUpdateDTO))
        ).andExpect(MockMvcResultMatchers.jsonPath("$.success").value(Matchers.is(true)));
    }

    @Test
    public void delete() throws Exception {
        StudentVO studentVO = studentTestHelper.saveStudentExample();
        restMockMvc.perform(
                MockMvcRequestBuilders
                        .delete(JsolaConstants.API_PATH + "/v1/students/{studentId}", studentVO.getId())
                        .contentType(APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.jsonPath("$.success").value(Matchers.is(true)));
    }

    @Test
    public void deleteBatch() throws Exception {
        StudentVO studentVO = studentTestHelper.saveStudentExample();
        restMockMvc.perform(
                MockMvcRequestBuilders
                        .delete(JsolaConstants.API_PATH + "/v1/students")
                        .content(JsonKit.toJSONString(Lists.newArrayList(studentVO.getId())))
                        .contentType(APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.jsonPath("$.success").value(Matchers.is(true)));
    }
    @Test
    public void findById() throws Exception {
        StudentVO studentVO = studentTestHelper.saveStudentExample();
        restMockMvc.perform(
                MockMvcRequestBuilders
                        .get(JsolaConstants.API_PATH + "/v1/students/{studentId}", studentVO.getId())
                        .contentType(APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.jsonPath("$.success").value(Matchers.is(true)));
    }

    @Test
    public void listAll() throws Exception {
        StudentVO studentVO = studentTestHelper.saveStudentExample();
        StudentQuery studentQuery = studentTestHelper.getStudentQuery();
        MockHttpServletRequestBuilder requestBuilder = createGetRequestBuilder(JsolaConstants.API_PATH + "/v1/students/listAll",studentQuery);
        restMockMvc.perform(requestBuilder).andExpect(MockMvcResultMatchers.jsonPath("$.success").value(Matchers.is(true)));
    }

    @Test
    public void page() throws Exception {
        StudentVO studentVO = studentTestHelper.saveStudentExample();
        StudentQuery studentQuery = studentTestHelper.getStudentQuery();
        MockHttpServletRequestBuilder requestBuilder = createGetRequestBuilder(JsolaConstants.API_PATH + "/v1/students",studentQuery);
        restMockMvc.perform(requestBuilder).andExpect(MockMvcResultMatchers.jsonPath("$.success").value(Matchers.is(true)));
    }



}

