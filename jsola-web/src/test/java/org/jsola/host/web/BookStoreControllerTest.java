package org.jsola.host.web;

import com.google.common.collect.Lists;
import org.hamcrest.Matchers;
import org.jsola.host.constant.JsolaConstants;
import org.jsola.host.core.AbstractWebTest;
import org.jsola.host.dto.BookStoreAddDTO;
import org.jsola.host.dto.BookStoreUpdateDTO;
import org.jsola.host.helper.BookStoreTestHelper;
import org.jsola.host.query.BookStoreQuery;
import org.jsola.host.service.IBookStoreService;
import org.jsola.host.vo.BookStoreVO;
import org.jsola.json.JsonKit;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 * 书籍信息表
 *
 * @author huangrun
 */
public class BookStoreControllerTest extends AbstractWebTest {

    @Autowired
    private BookStoreTestHelper bookStoreTestHelper;
    @Autowired
    private IBookStoreService bookStoreService;

    @Test
    public void save() throws Exception {
        BookStoreAddDTO bookStoreAddDTO = bookStoreTestHelper.getBookStoreAddDTO();
        restMockMvc.perform(
                MockMvcRequestBuilders
                        .post(JsolaConstants.API_PATH + "/v1/bookStores")
                        .contentType(APPLICATION_JSON_UTF8)
                        .content(JsonKit.toJSONString(bookStoreAddDTO))
        ).andExpect(MockMvcResultMatchers.jsonPath("$.success").value(Matchers.is(true)));
    }

    @Test
    public void update() throws Exception {
        BookStoreVO bookStoreVO = bookStoreTestHelper.saveBookStoreExample();
        BookStoreUpdateDTO bookStoreUpdateDTO = bookStoreTestHelper.getBookStoreUpdateDTO(bookStoreVO);
        restMockMvc.perform(
                MockMvcRequestBuilders
                        .put(JsolaConstants.API_PATH + "/v1/bookStores")
                        .contentType(APPLICATION_JSON_UTF8)
                        .content(JsonKit.toJSONString(bookStoreUpdateDTO))
        ).andExpect(MockMvcResultMatchers.jsonPath("$.success").value(Matchers.is(true)));
    }

    @Test
    public void delete() throws Exception {
        BookStoreVO bookStoreVO = bookStoreTestHelper.saveBookStoreExample();
        restMockMvc.perform(
                MockMvcRequestBuilders
                        .delete(JsolaConstants.API_PATH + "/v1/bookStores/{bookStoreId}", bookStoreVO.getId())
                        .contentType(APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.jsonPath("$.success").value(Matchers.is(true)));
    }

    @Test
    public void deleteBatch() throws Exception {
        BookStoreVO bookStoreVO = bookStoreTestHelper.saveBookStoreExample();
        restMockMvc.perform(
                MockMvcRequestBuilders
                        .delete(JsolaConstants.API_PATH + "/v1/bookStores")
                        .content(JsonKit.toJSONString(Lists.newArrayList(bookStoreVO.getId())))
                        .contentType(APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.jsonPath("$.success").value(Matchers.is(true)));
    }
    @Test
    public void findById() throws Exception {
        BookStoreVO bookStoreVO = bookStoreTestHelper.saveBookStoreExample();
        restMockMvc.perform(
                MockMvcRequestBuilders
                        .get(JsolaConstants.API_PATH + "/v1/bookStores/{bookStoreId}", bookStoreVO.getId())
                        .contentType(APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.jsonPath("$.success").value(Matchers.is(true)));
    }

    @Test
    public void listAll() throws Exception {
        BookStoreVO bookStoreVO = bookStoreTestHelper.saveBookStoreExample();
        BookStoreQuery bookStoreQuery = bookStoreTestHelper.getBookStoreQuery();
        MockHttpServletRequestBuilder requestBuilder = createGetRequestBuilder(JsolaConstants.API_PATH + "/v1/bookStores/listAll",bookStoreQuery);
        restMockMvc.perform(requestBuilder).andExpect(MockMvcResultMatchers.jsonPath("$.success").value(Matchers.is(true)));
    }

    @Test
    public void page() throws Exception {
        BookStoreVO bookStoreVO = bookStoreTestHelper.saveBookStoreExample();
        BookStoreQuery bookStoreQuery = bookStoreTestHelper.getBookStoreQuery();
        MockHttpServletRequestBuilder requestBuilder = createGetRequestBuilder(JsolaConstants.API_PATH + "/v1/bookStores",bookStoreQuery);
        restMockMvc.perform(requestBuilder).andExpect(MockMvcResultMatchers.jsonPath("$.success").value(Matchers.is(true)));
    }



}

