package org.jsola.host.helper;

import org.jsola.common.SafeKit;
import org.jsola.host.dto.BookStoreAddDTO;
import org.jsola.host.dto.BookStoreUpdateDTO;
import org.jsola.host.entity.BookStoreDO;
import org.jsola.host.query.BookStoreQuery;
import org.jsola.host.service.IBookStoreService;
import org.jsola.host.vo.BookStoreVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;

import static org.jsola.host.core.AbstractTest.SITE_ID;



/**
 * 书籍信息表
 *
 * @author huangrun
 */
@Component
public class BookStoreTestHelper extends BaseTestHelper {
    
    @Autowired
    private IBookStoreService bookStoreService;

    /**
     * 生成add测试数据
     * @return BookStoreAddDTO
     */
    public BookStoreAddDTO getBookStoreAddDTO(){
        BookStoreAddDTO bookStoreAddDTO = new BookStoreAddDTO();
        bookStoreAddDTO.setBookName(getRandomValue("varchar",20));
        bookStoreAddDTO.setAuthor(getRandomValue("varchar",20));
        bookStoreAddDTO.setTranslator(getRandomValue("varchar",20));
        bookStoreAddDTO.setBookman(getRandomValue("varchar",30));
        bookStoreAddDTO.setDoubanScore(SafeKit.getBigDecimal(getRandomValue("decimal",2)));
        return bookStoreAddDTO;
    }


    /**
     * 生成update测试数据
     * @return BookStoreUpdateDTO
     */
    public BookStoreUpdateDTO getBookStoreUpdateDTO(BookStoreVO bookStoreVO){
        return bookStoreService.selectDOById(bookStoreVO.getId(), SITE_ID).to(BookStoreUpdateDTO.class);
    }

    /**
     * 生成查询数据
     * @return BookStoreQuery
     */
    public BookStoreQuery getBookStoreQuery() {
        BookStoreQuery bookStoreQuery = new BookStoreQuery();
        List<BookStoreDO> bookStoreDOList = bookStoreService.list(new BookStoreDO(),SITE_ID);
        if (!CollectionUtils.isEmpty(bookStoreDOList)) {
            BookStoreDO bookStoreDO = bookStoreDOList.get(0);
            bookStoreQuery = bookStoreDO.to(BookStoreQuery.class);
        }
        return bookStoreQuery;
    }

    /**
     * 保存示例
     * @return BookStoreVO
     */
    public BookStoreVO saveBookStoreExample(){
        BookStoreAddDTO addDTO = this.getBookStoreAddDTO();
        BookStoreVO bookStoreVO = bookStoreService.save(addDTO,buildTokenUser());
        return bookStoreVO;
    }


}

