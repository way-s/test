package org.jsola.host.helper;

import org.jsola.user.core.TokenUser;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.jsola.common.DateKit;

import java.util.Date;
import static org.jsola.host.core.AbstractTest.CURR_USER_ID;
import static org.jsola.host.core.AbstractTest.SITE_ID;


/**
 * @author huangrun
 */
public abstract class BaseTestHelper {

    public static TokenUser buildTokenUser(){
        TokenUser tokenUser = new TokenUser();
        tokenUser.setSiteId(SITE_ID);
        tokenUser.setUserId(CURR_USER_ID);
        tokenUser.setSiteName("所属站点");
        tokenUser.setUserName("张三");
        return tokenUser;
    }

     /**
     * 随机获取测试数据
     * @param dbType 字段类型
     * @param dbLength 字段长度
     * @return 测试数据
     */
    public static String getRandomValue(String dbType, int dbLength) {
        dbType = dbType.toUpperCase();
        switch (dbType) {
            case "VARCHAR":
            case "CHAR":
            case "TEXT":
                return getRandomStr();
            case "DATE":
            case "TIMESTAMP":
            case "DATETIME":
                return getRandomDate();
            case "FLOAT":
            case "DOUBLE":
            case "DECIMAL":
            case "BIGINT":
            case "SMALLINT":
            case "MEDIUMINT":
            case "INT":
                return getRandomNum();
            case "TINYINT":
                if (dbLength == 1) {
                    return RandomUtils.nextBoolean() + "";
                } else {
                    return getRandomNum();
                }
        }
        return getRandomStr();
    }

    /**
     * 随机获取字符串
     * @return 字符串
     */
    private static String getRandomStr() {
        return RandomStringUtils.random(4,true,true);
    }

    /**
     * 随机获取数字字符串
     * @return 数字字符串
     */
    private static String getRandomNum() {
        return RandomStringUtils.random(2,false,true);
    }

    /**
     * 随机获取日期字符串
     * @return 日期字符串
     */
    private static String getRandomDate() {
        return DateKit.getDateTimeStr(
                DateKit.getBeforeDay(new Date(), RandomUtils.nextInt())
        );
    }

    /**
    * 随机获取邮箱
    * @return 邮箱字符串
    */
    static String getRandomEmail() {
        return "janusmix2013@gmail.com";
    }

    /**
    * 随机获取手机号
    * @return 手机号字符串
    */
    static String getRandomMobile() {
        return "13888888888";
    }

    /**
    * 随机获取电话号
    * @return 电话号字符串
    */
    static String getRandomPhone() {
        return "8888888";
    }

    /**
    * 随机获取身份证号
    * @return 身份证号字符串
    */
    static String getRandomIdCard() {
        return "37070219900101323X";
    }
}
