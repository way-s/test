package org.jsola.host.helper;

import org.jsola.host.dto.AdminAddDTO;
import org.jsola.host.dto.AdminUpdateDTO;
import org.jsola.host.entity.AdminDO;
import org.jsola.host.query.AdminQuery;
import org.jsola.host.service.IAdminService;
import org.jsola.host.vo.AdminVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;

import static org.jsola.host.core.AbstractTest.SITE_ID;



/**
 * 管理员
 *
 * @author huangrun
 */
@Component
public class AdminTestHelper extends BaseTestHelper {
    
    @Autowired
    private IAdminService adminService;

    /**
     * 生成add测试数据
     * @return AdminAddDTO
     */
    public AdminAddDTO getAdminAddDTO(){
        AdminAddDTO adminAddDTO = new AdminAddDTO();
        adminAddDTO.setName(getRandomValue("varchar",12));
        adminAddDTO.setPwd(getRandomValue("varchar",16));
        adminAddDTO.setTel(getRandomValue("varchar",11));
        return adminAddDTO;
    }


    /**
     * 生成update测试数据
     * @return AdminUpdateDTO
     */
    public AdminUpdateDTO getAdminUpdateDTO(AdminVO adminVO){
        return adminService.selectDOById(adminVO.getId(), SITE_ID).to(AdminUpdateDTO.class);
    }

    /**
     * 生成查询数据
     * @return AdminQuery
     */
    public AdminQuery getAdminQuery() {
        AdminQuery adminQuery = new AdminQuery();
        List<AdminDO> adminDOList = adminService.list(new AdminDO(),SITE_ID);
        if (!CollectionUtils.isEmpty(adminDOList)) {
            AdminDO adminDO = adminDOList.get(0);
            adminQuery = adminDO.to(AdminQuery.class);
        }
        return adminQuery;
    }

    /**
     * 保存示例
     * @return AdminVO
     */
    public AdminVO saveAdminExample(){
        AdminAddDTO addDTO = this.getAdminAddDTO();
        AdminVO adminVO = adminService.save(addDTO,buildTokenUser());
        return adminVO;
    }


}

