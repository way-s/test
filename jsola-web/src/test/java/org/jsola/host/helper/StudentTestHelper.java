package org.jsola.host.helper;

import org.jsola.common.SafeKit;
import org.jsola.host.dto.StudentAddDTO;
import org.jsola.host.dto.StudentUpdateDTO;
import org.jsola.host.entity.StudentDO;
import org.jsola.host.query.StudentQuery;
import org.jsola.host.service.IStudentService;
import org.jsola.host.vo.StudentVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;

import static org.jsola.host.core.AbstractTest.SITE_ID;



/**
 * 学生表
 *
 * @author huangrun
 */
@Component
public class StudentTestHelper extends BaseTestHelper {
    
    @Autowired
    private IStudentService studentService;

    /**
     * 生成add测试数据
     * @return StudentAddDTO
     */
    public StudentAddDTO getStudentAddDTO(){
        StudentAddDTO studentAddDTO = new StudentAddDTO();
        studentAddDTO.setTimeOut(SafeKit.getInteger(getRandomValue("int",1)));
        studentAddDTO.setBrrCount(SafeKit.getInteger(getRandomValue("int",11)));
        studentAddDTO.setName(getRandomValue("varchar",12));
        studentAddDTO.setPwd(getRandomValue("varchar",16));
        studentAddDTO.setCard(getRandomValue("varchar",1));
        studentAddDTO.setTel(getRandomValue("varchar",11));
        return studentAddDTO;
    }


    /**
     * 生成update测试数据
     * @return StudentUpdateDTO
     */
    public StudentUpdateDTO getStudentUpdateDTO(StudentVO studentVO){
        return studentService.selectDOById(studentVO.getId(), SITE_ID).to(StudentUpdateDTO.class);
    }

    /**
     * 生成查询数据
     * @return StudentQuery
     */
    public StudentQuery getStudentQuery() {
        StudentQuery studentQuery = new StudentQuery();
        List<StudentDO> studentDOList = studentService.list(new StudentDO(),SITE_ID);
        if (!CollectionUtils.isEmpty(studentDOList)) {
            StudentDO studentDO = studentDOList.get(0);
            studentQuery = studentDO.to(StudentQuery.class);
        }
        return studentQuery;
    }

    /**
     * 保存示例
     * @return StudentVO
     */
    public StudentVO saveStudentExample(){
        StudentAddDTO addDTO = this.getStudentAddDTO();
        StudentVO studentVO = studentService.save(addDTO,buildTokenUser());
        return studentVO;
    }


}

