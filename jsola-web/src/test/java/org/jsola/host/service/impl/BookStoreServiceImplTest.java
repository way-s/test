package org.jsola.host.service.impl;

import org.jsola.core.Page;
import org.jsola.host.core.AbstractTest;
import org.jsola.host.dao.IBookStoreDAO;
import org.jsola.host.dto.BookStoreUpdateDTO;
import org.jsola.host.entity.BookStoreDO;
import org.jsola.host.helper.BookStoreTestHelper;
import org.jsola.host.query.BookStoreQuery;
import org.jsola.host.service.IBookStoreService;
import org.jsola.host.vo.BookStoreListVO;
import org.jsola.host.vo.BookStoreVO;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;


/**
 * 书籍信息表Service层单元测试
 * @author huangrun
 */
public class BookStoreServiceImplTest extends AbstractTest {

    @Autowired
    private IBookStoreService bookStoreService;

    @Autowired
    private BookStoreTestHelper bookStoreTestHelper;

    @Autowired
    private IBookStoreDAO bookStoreDAO;


    @Test
    public void save() {
        BookStoreVO bookStoreVO = bookStoreTestHelper.saveBookStoreExample();
        Assert.assertNotNull(bookStoreVO);
        Assert.assertNotNull(bookStoreVO.getId());
        BookStoreVO bookStoreVO1 = bookStoreService.selectById(bookStoreVO.getId(),SITE_ID);
        Assert.assertNotNull(bookStoreVO1);
        Assert.assertNotNull(bookStoreVO1.getId());
    }

    @Test
    public void update() {
        BookStoreVO bookStoreVO = bookStoreTestHelper.saveBookStoreExample();
        BookStoreUpdateDTO bookStoreUpdateDTO = bookStoreTestHelper.getBookStoreUpdateDTO(bookStoreVO);
        int count = bookStoreService.update(bookStoreUpdateDTO,getTokenUser());
        Assert.assertEquals(count,1);
    }

    @Test
    public void deleteByIds() {
        BookStoreVO bookStoreVO = bookStoreTestHelper.saveBookStoreExample();
        int count = bookStoreService.deleteByIds(getTokenUser(),bookStoreVO.getId());
        Assert.assertEquals(count,1);
        BookStoreVO bookStoreVO1 = bookStoreService.selectById(bookStoreVO.getId(),SITE_ID);
        Assert.assertNull(bookStoreVO1);
    }

    @Test
    public void selectById() {
        BookStoreVO bookStoreVO = bookStoreTestHelper.saveBookStoreExample();
        Assert.assertNotNull(bookStoreVO);
        Assert.assertNotNull(bookStoreVO.getId());
        BookStoreVO bookStoreVO1 = bookStoreService.selectById(bookStoreVO.getId(),SITE_ID);
        Assert.assertNotNull(bookStoreVO1);
        Assert.assertNotNull(bookStoreVO1.getId());
    }

    @Test
    public void select() {
        bookStoreTestHelper.saveBookStoreExample();
        BookStoreQuery bookStoreQuery = bookStoreTestHelper.getBookStoreQuery();
        List<BookStoreListVO> bookStoreListVOList = bookStoreService.select(bookStoreQuery,SITE_ID);
        Assert.assertNotNull(bookStoreListVOList);
        Assert.assertNotEquals(0,bookStoreListVOList.size());
    }

    @Test
    public void selectPage() {
        bookStoreTestHelper.saveBookStoreExample();
        BookStoreQuery bookStoreQuery = bookStoreTestHelper.getBookStoreQuery();
        Page<BookStoreListVO> page = bookStoreService.selectPage(bookStoreQuery,SITE_ID);
        Assert.assertNotNull(page);
        Assert.assertNotEquals(0,page.getEntityCount());
    }
}

