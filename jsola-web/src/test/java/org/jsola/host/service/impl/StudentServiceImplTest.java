package org.jsola.host.service.impl;

import org.jsola.core.Page;
import org.jsola.host.core.AbstractTest;
import org.jsola.host.dao.IStudentDAO;
import org.jsola.host.dto.StudentUpdateDTO;
import org.jsola.host.entity.StudentDO;
import org.jsola.host.helper.StudentTestHelper;
import org.jsola.host.query.StudentQuery;
import org.jsola.host.service.IStudentService;
import org.jsola.host.vo.StudentListVO;
import org.jsola.host.vo.StudentVO;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;


/**
 * 学生表Service层单元测试
 * @author huangrun
 */
public class StudentServiceImplTest extends AbstractTest {

    @Autowired
    private IStudentService studentService;

    @Autowired
    private StudentTestHelper studentTestHelper;

    @Autowired
    private IStudentDAO studentDAO;


    @Test
    public void save() {
        StudentVO studentVO = studentTestHelper.saveStudentExample();
        Assert.assertNotNull(studentVO);
        Assert.assertNotNull(studentVO.getId());
        StudentVO studentVO1 = studentService.selectById(studentVO.getId(),SITE_ID);
        Assert.assertNotNull(studentVO1);
        Assert.assertNotNull(studentVO1.getId());
    }

    @Test
    public void update() {
        StudentVO studentVO = studentTestHelper.saveStudentExample();
        StudentUpdateDTO studentUpdateDTO = studentTestHelper.getStudentUpdateDTO(studentVO);
        int count = studentService.update(studentUpdateDTO,getTokenUser());
        Assert.assertEquals(count,1);
    }

    @Test
    public void deleteByIds() {
        StudentVO studentVO = studentTestHelper.saveStudentExample();
        int count = studentService.deleteByIds(getTokenUser(),studentVO.getId());
        Assert.assertEquals(count,1);
        StudentVO studentVO1 = studentService.selectById(studentVO.getId(),SITE_ID);
        Assert.assertNull(studentVO1);
    }

    @Test
    public void selectById() {
        StudentVO studentVO = studentTestHelper.saveStudentExample();
        Assert.assertNotNull(studentVO);
        Assert.assertNotNull(studentVO.getId());
        StudentVO studentVO1 = studentService.selectById(studentVO.getId(),SITE_ID);
        Assert.assertNotNull(studentVO1);
        Assert.assertNotNull(studentVO1.getId());
    }

    @Test
    public void select() {
        studentTestHelper.saveStudentExample();
        StudentQuery studentQuery = studentTestHelper.getStudentQuery();
        List<StudentListVO> studentListVOList = studentService.select(studentQuery,SITE_ID);
        Assert.assertNotNull(studentListVOList);
        Assert.assertNotEquals(0,studentListVOList.size());
    }

    @Test
    public void selectPage() {
        studentTestHelper.saveStudentExample();
        StudentQuery studentQuery = studentTestHelper.getStudentQuery();
        Page<StudentListVO> page = studentService.selectPage(studentQuery,SITE_ID);
        Assert.assertNotNull(page);
        Assert.assertNotEquals(0,page.getEntityCount());
    }
}

