package org.jsola.host.service.impl;

import org.jsola.core.Page;
import org.jsola.host.core.AbstractTest;
import org.jsola.host.dao.IAdminDAO;
import org.jsola.host.dto.AdminUpdateDTO;
import org.jsola.host.entity.AdminDO;
import org.jsola.host.helper.AdminTestHelper;
import org.jsola.host.query.AdminQuery;
import org.jsola.host.service.IAdminService;
import org.jsola.host.vo.AdminListVO;
import org.jsola.host.vo.AdminVO;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;


/**
 * 管理员Service层单元测试
 * @author huangrun
 */
public class AdminServiceImplTest extends AbstractTest {

    @Autowired
    private IAdminService adminService;

    @Autowired
    private AdminTestHelper adminTestHelper;

    @Autowired
    private IAdminDAO adminDAO;


    @Test
    public void save() {
        AdminVO adminVO = adminTestHelper.saveAdminExample();
        Assert.assertNotNull(adminVO);
        Assert.assertNotNull(adminVO.getId());
        AdminVO adminVO1 = adminService.selectById(adminVO.getId(),SITE_ID);
        Assert.assertNotNull(adminVO1);
        Assert.assertNotNull(adminVO1.getId());
    }

    @Test
    public void update() {
        AdminVO adminVO = adminTestHelper.saveAdminExample();
        AdminUpdateDTO adminUpdateDTO = adminTestHelper.getAdminUpdateDTO(adminVO);
        int count = adminService.update(adminUpdateDTO,getTokenUser());
        Assert.assertEquals(count,1);
    }

    @Test
    public void deleteByIds() {
        AdminVO adminVO = adminTestHelper.saveAdminExample();
        int count = adminService.deleteByIds(getTokenUser(),adminVO.getId());
        Assert.assertEquals(count,1);
        AdminVO adminVO1 = adminService.selectById(adminVO.getId(),SITE_ID);
        Assert.assertNull(adminVO1);
    }

    @Test
    public void selectById() {
        AdminVO adminVO = adminTestHelper.saveAdminExample();
        Assert.assertNotNull(adminVO);
        Assert.assertNotNull(adminVO.getId());
        AdminVO adminVO1 = adminService.selectById(adminVO.getId(),SITE_ID);
        Assert.assertNotNull(adminVO1);
        Assert.assertNotNull(adminVO1.getId());
    }

    @Test
    public void select() {
        adminTestHelper.saveAdminExample();
        AdminQuery adminQuery = adminTestHelper.getAdminQuery();
        List<AdminListVO> adminListVOList = adminService.select(adminQuery,SITE_ID);
        Assert.assertNotNull(adminListVOList);
        Assert.assertNotEquals(0,adminListVOList.size());
    }

    @Test
    public void selectPage() {
        adminTestHelper.saveAdminExample();
        AdminQuery adminQuery = adminTestHelper.getAdminQuery();
        Page<AdminListVO> page = adminService.selectPage(adminQuery,SITE_ID);
        Assert.assertNotNull(page);
        Assert.assertNotEquals(0,page.getEntityCount());
    }
}

