package org.jsola.host.query;

import io.swagger.annotations.ApiParam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;
import org.jsola.core.query.BaseQuery;
import org.jsola.orm.filter.Order;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * 学生表
 *
 * @author huangrun
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class StudentQuery extends BaseQuery {

    private static final long serialVersionUID = 1L;



    @ApiParam(value = "排序字段", allowMultiple=true, type="Order" ,example = "\"orders\": [{\"property\": \"userName\",\"desc\": false}]")
    private List<Order> orders;

    /**
     * 每页的条数
     */
    @ApiParam(value = "分页参数，每页的条数", example = "20")
    @Max(value = 1000, message = "pageSize不能大于1000")
    private Integer pageSize = 20;

    /**
     * 当前第几页
     */
    @ApiParam(value = "分页参数，第几页", example = "1")
    @Min(value = 1, message = "pageNo不能小于1")
    private Integer pageNo = 1;



    /**
     * 添加排序
     * @param propertyName 属性名
     * @param desc 是否降序
     * @return this
     */
    public StudentQuery addOrder(String propertyName, boolean desc) {
        if (CollectionUtils.isEmpty(this.orders)) {
            this.orders = new ArrayList<>();
        }
        this.orders.add(new Order(propertyName,desc));
        return this;
    }

}

