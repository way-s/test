package org.jsola.host.constant;

import org.jsola.core.validator.IConst;
import org.jsola.json.JsonKit;

import java.util.HashMap;
import java.util.Map;

/**
 * 学生
 *
 * @author huangrun
 */
public enum StudentType implements IConst {

    /**
     * 借书本数
     */
    COUNT_NUMBER("0","借书本数");

    private final String value;
    private final String desc;
    /**
     * 枚举值罗列，给swagger接口文档展示用
     */
    public static final String VALUES_STR = "0";

     /**
     * 缓存所有value
     */
    private static final Map<String, StudentType> LOOKUP = new HashMap<>();

    static {
        for (StudentType e : StudentType.values()) {
            LOOKUP.put(e.value, e);
        }
    }


    StudentType(String value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    /**
     * 根据value查找StudentType
     * @param value value
     * @return StudentType
     */
    public static StudentType find(String value) {
        return LOOKUP.get(value);
    }

    /**
     * 根据desc查找StudentType
     * @param desc desc
     * @return StudentType
     */
    public static StudentType findByDesc(String desc){
        for (StudentType e : StudentType.values()) {
            if(e.getDesc().equals(desc)){
                return e;
            }
        }
        return null;
    }

    /**
     * 所有枚举转json
     * @return json
     */
    public static String getAllToJson() {
        Map<String,Object> map = new HashMap<>();
        for (StudentType e : StudentType.values()) {
            map.put("value", e.value);
            map.put("desc", e.desc);
        }
        return JsonKit.toJSONString(map);
    }

    @Override
    public boolean check(Object obj) {
        String value = (String) obj;
        return find(value) != null;
    }

    public String getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }
}
