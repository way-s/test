package org.jsola.host.constant;


/**
 * @author huangrun
 */
public class JsolaPermissionConstants {

    /**
     * 超级管理员subjectType
     */
    public static final String SYSTEM_SUBJECT_TYPE = "system";

    /**
     * 默认subjectId
     */
    public static final String JSOLA_DEFAULT_SUBJECT_ID = "0";

    /**
     * 权限校验失败的提示
     */
    public static final String PERMISSION_DENIED_MESSAGE = "没有操作权限";


}
