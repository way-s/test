package org.jsola.host.constant;


/**
 * @author huangrun
 */
public class JsolaConstants {

    /**
     * 请求根路径
     */
    public static final String CONTEXT_PATH = "/jsola";

    /**
     * 外部地址
     */
    public static final String API_PATH = CONTEXT_PATH + "/api";

    /**
     * 内部地址
     */
    public static final String PROVIDER_PATH = CONTEXT_PATH + "/provider";

    /**
     * 事务beanName
     */
    public static final String TX = "jsolaTx";

    /**
     * SqlSessionFactory beanName
     */
    public static final String SQL_SESSION_FACTORY_BEAN_NAME = "jsolaSqlSessionFactory";

}
