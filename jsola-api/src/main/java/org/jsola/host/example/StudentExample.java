package org.jsola.host.example;

import org.jsola.core.example.BaseExample;

/**
 * 学生表
 *
 * @author huangrun
 */
public class StudentExample extends BaseExample {

    public static final String N_TIME_OUT = "超时";
    public static final String E_TIME_OUT = "1";
    public static final String M_TIME_OUT_NOT_NULL = N_TIME_OUT + "不能为null";
    public static final String N_BRR_COUNT = "计数";
    public static final String E_BRR_COUNT = "1";
    public static final String M_BRR_COUNT_NOT_NULL = N_BRR_COUNT + "不能为null";
    public static final String N_NAME = "名字";
    public static final String E_NAME = "name1";
    public static final String M_NAME_MAX = N_NAME + "长度不能超过12";
    public static final String M_NAME_NOT_NULL = N_NAME + "不能为null";
    public static final String N_PWD = "密码";
    public static final String E_PWD = "";
    public static final String M_PWD_MAX = N_PWD + "长度不能超过16";
    public static final String M_PWD_NOT_NULL = N_PWD + "不能为null";
    public static final String N_CARD = "卡 1 有,0 没有";
    public static final String E_CARD = "";
    public static final String M_CARD_MAX = N_CARD + "长度不能超过1";
    public static final String N_TEL = "学号";
    public static final String E_TEL = "";
    public static final String M_TEL_MAX = N_TEL + "长度不能超过11";
    public static final String M_TEL_NOT_NULL = N_TEL + "不能为null";

}

