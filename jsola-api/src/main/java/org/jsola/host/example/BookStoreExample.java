package org.jsola.host.example;

import org.jsola.core.example.BaseExample;

/**
 * 书籍信息表
 *
 * @author huangrun
 */
public class BookStoreExample extends BaseExample {

    public static final String N_BOOK_NAME = "书名";
    public static final String E_BOOK_NAME = "name1";
    public static final String M_BOOK_NAME_MAX = N_BOOK_NAME + "长度不能超过20";
    public static final String M_BOOK_NAME_NOT_NULL = N_BOOK_NAME + "不能为null";
    public static final String N_AUTHOR = "作者";
    public static final String E_AUTHOR = "";
    public static final String M_AUTHOR_MAX = N_AUTHOR + "长度不能超过20";
    public static final String M_AUTHOR_NOT_NULL = N_AUTHOR + "不能为null";
    public static final String N_TRANSLATOR = "翻译";
    public static final String E_TRANSLATOR = "";
    public static final String M_TRANSLATOR_MAX = N_TRANSLATOR + "长度不能超过20";
    public static final String N_BOOKMAN = "发行社";
    public static final String E_BOOKMAN = "";
    public static final String M_BOOKMAN_MAX = N_BOOKMAN + "长度不能超过30";
    public static final String M_BOOKMAN_NOT_NULL = N_BOOKMAN + "不能为null";
    public static final String N_DOUBAN_SCORE_START = "发行时间开始范围";
    public static final String E_DOUBAN_SCORE_START = "0";
    public static final String N_DOUBAN_SCORE_END = "发行时间结束范围";
    public static final String E_DOUBAN_SCORE_END = "1";
    public static final String N_DOUBAN_SCORE = "发行时间";
    public static final String E_DOUBAN_SCORE = "1";

}

