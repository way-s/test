package org.jsola.host.example;

import org.jsola.core.example.BaseExample;

/**
 * 管理员
 *
 * @author huangrun
 */
public class AdminExample extends BaseExample {

    public static final String N_NAME = "名字";
    public static final String E_NAME = "name1";
    public static final String M_NAME_MAX = N_NAME + "长度不能超过12";
    public static final String M_NAME_NOT_NULL = N_NAME + "不能为null";
    public static final String N_PWD = "密码";
    public static final String E_PWD = "";
    public static final String M_PWD_MAX = N_PWD + "长度不能超过16";
    public static final String M_PWD_NOT_NULL = N_PWD + "不能为null";
    public static final String N_TEL = "编号";
    public static final String E_TEL = "";
    public static final String M_TEL_MAX = N_TEL + "长度不能超过11";
    public static final String M_TEL_NOT_NULL = N_TEL + "不能为null";

}

