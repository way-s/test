package org.jsola.host.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;
import org.jsola.core.dto.BaseDTO;

import java.math.BigDecimal;
import javax.validation.constraints.NotNull;

import static org.jsola.core.example.BaseExample.E_ID;
import static org.jsola.core.example.BaseExample.M_ID_NOT_NULL;
import static org.jsola.core.example.BaseExample.N_ID;
import static org.jsola.host.example.BookStoreExample.E_AUTHOR;
import static org.jsola.host.example.BookStoreExample.E_BOOKMAN;
import static org.jsola.host.example.BookStoreExample.E_BOOK_NAME;
import static org.jsola.host.example.BookStoreExample.E_DOUBAN_SCORE;
import static org.jsola.host.example.BookStoreExample.E_TRANSLATOR;
import static org.jsola.host.example.BookStoreExample.M_AUTHOR_MAX;
import static org.jsola.host.example.BookStoreExample.M_AUTHOR_NOT_NULL;
import static org.jsola.host.example.BookStoreExample.M_BOOKMAN_MAX;
import static org.jsola.host.example.BookStoreExample.M_BOOKMAN_NOT_NULL;
import static org.jsola.host.example.BookStoreExample.M_BOOK_NAME_MAX;
import static org.jsola.host.example.BookStoreExample.M_BOOK_NAME_NOT_NULL;
import static org.jsola.host.example.BookStoreExample.M_TRANSLATOR_MAX;
import static org.jsola.host.example.BookStoreExample.N_AUTHOR;
import static org.jsola.host.example.BookStoreExample.N_BOOKMAN;
import static org.jsola.host.example.BookStoreExample.N_BOOK_NAME;
import static org.jsola.host.example.BookStoreExample.N_DOUBAN_SCORE;
import static org.jsola.host.example.BookStoreExample.N_TRANSLATOR;

/**
 * 书籍信息表
 *
 * @author huangrun
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "修改【书籍信息表】的参数")
@Data
public class BookStoreUpdateDTO extends BaseDTO {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(notes = N_ID, example = E_ID, required = true)
    @NotNull(message = M_ID_NOT_NULL)
    private Long id;

    @ApiModelProperty(notes = N_BOOK_NAME, example = E_BOOK_NAME, required = true)
    @NotNull(message = M_BOOK_NAME_NOT_NULL)
    @Length(max = 20, message = M_BOOK_NAME_MAX)
    private String bookName;

    @ApiModelProperty(notes = N_AUTHOR, example = E_AUTHOR, required = true)
    @NotNull(message = M_AUTHOR_NOT_NULL)
    @Length(max = 20, message = M_AUTHOR_MAX)
    private String author;

    @ApiModelProperty(notes = N_TRANSLATOR, example = E_TRANSLATOR)
    @Length(max = 20, message = M_TRANSLATOR_MAX)
    private String translator;

    @ApiModelProperty(notes = N_BOOKMAN, example = E_BOOKMAN, required = true)
    @NotNull(message = M_BOOKMAN_NOT_NULL)
    @Length(max = 30, message = M_BOOKMAN_MAX)
    private String bookman;

    @ApiModelProperty(notes = N_DOUBAN_SCORE, example = E_DOUBAN_SCORE)
    private BigDecimal doubanScore;


}

