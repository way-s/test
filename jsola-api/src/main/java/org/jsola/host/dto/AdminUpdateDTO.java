package org.jsola.host.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;
import org.jsola.core.dto.BaseDTO;

import javax.validation.constraints.NotNull;

import static org.jsola.core.example.BaseExample.E_ID;
import static org.jsola.core.example.BaseExample.M_ID_NOT_NULL;
import static org.jsola.core.example.BaseExample.N_ID;
import static org.jsola.host.example.AdminExample.E_NAME;
import static org.jsola.host.example.AdminExample.E_PWD;
import static org.jsola.host.example.AdminExample.E_TEL;
import static org.jsola.host.example.AdminExample.M_NAME_MAX;
import static org.jsola.host.example.AdminExample.M_NAME_NOT_NULL;
import static org.jsola.host.example.AdminExample.M_PWD_MAX;
import static org.jsola.host.example.AdminExample.M_PWD_NOT_NULL;
import static org.jsola.host.example.AdminExample.M_TEL_MAX;
import static org.jsola.host.example.AdminExample.M_TEL_NOT_NULL;
import static org.jsola.host.example.AdminExample.N_NAME;
import static org.jsola.host.example.AdminExample.N_PWD;
import static org.jsola.host.example.AdminExample.N_TEL;

/**
 * 管理员
 *
 * @author huangrun
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "修改【管理员】的参数")
@Data
public class AdminUpdateDTO extends BaseDTO {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(notes = N_ID, example = E_ID, required = true)
    @NotNull(message = M_ID_NOT_NULL)
    private Long id;

    @ApiModelProperty(notes = N_NAME, example = E_NAME, required = true)
    @NotNull(message = M_NAME_NOT_NULL)
    @Length(max = 12, message = M_NAME_MAX)
    private String name;

    @ApiModelProperty(notes = N_PWD, example = E_PWD, required = true)
    @NotNull(message = M_PWD_NOT_NULL)
    @Length(max = 16, message = M_PWD_MAX)
    private String pwd;

    @ApiModelProperty(notes = N_TEL, example = E_TEL, required = true)
    @NotNull(message = M_TEL_NOT_NULL)
    @Length(max = 11, message = M_TEL_MAX)
    private String tel;


}

