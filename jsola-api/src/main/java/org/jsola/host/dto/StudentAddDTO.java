package org.jsola.host.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;
import org.jsola.core.dto.BaseDTO;

import javax.validation.constraints.NotNull;

import static org.jsola.host.example.StudentExample.E_BRR_COUNT;
import static org.jsola.host.example.StudentExample.E_CARD;
import static org.jsola.host.example.StudentExample.E_NAME;
import static org.jsola.host.example.StudentExample.E_PWD;
import static org.jsola.host.example.StudentExample.E_TEL;
import static org.jsola.host.example.StudentExample.E_TIME_OUT;
import static org.jsola.host.example.StudentExample.M_BRR_COUNT_NOT_NULL;
import static org.jsola.host.example.StudentExample.M_CARD_MAX;
import static org.jsola.host.example.StudentExample.M_NAME_MAX;
import static org.jsola.host.example.StudentExample.M_NAME_NOT_NULL;
import static org.jsola.host.example.StudentExample.M_PWD_MAX;
import static org.jsola.host.example.StudentExample.M_PWD_NOT_NULL;
import static org.jsola.host.example.StudentExample.M_TEL_MAX;
import static org.jsola.host.example.StudentExample.M_TEL_NOT_NULL;
import static org.jsola.host.example.StudentExample.M_TIME_OUT_NOT_NULL;
import static org.jsola.host.example.StudentExample.N_BRR_COUNT;
import static org.jsola.host.example.StudentExample.N_CARD;
import static org.jsola.host.example.StudentExample.N_NAME;
import static org.jsola.host.example.StudentExample.N_PWD;
import static org.jsola.host.example.StudentExample.N_TEL;
import static org.jsola.host.example.StudentExample.N_TIME_OUT;

/**
 * 学生表
 *
 * @author huangrun
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "新增【学生表】的参数")
@Data
public class StudentAddDTO extends BaseDTO {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(notes = N_TIME_OUT, example = E_TIME_OUT, required = true)
    @NotNull(message = M_TIME_OUT_NOT_NULL)
    private Integer timeOut;

    @ApiModelProperty(notes = N_BRR_COUNT, example = E_BRR_COUNT, required = true)
    @NotNull(message = M_BRR_COUNT_NOT_NULL)
    private Integer brrCount;

    @ApiModelProperty(notes = N_NAME, example = E_NAME, required = true)
    @NotNull(message = M_NAME_NOT_NULL)
    @Length(max = 12, message = M_NAME_MAX)
    private String name;

    @ApiModelProperty(notes = N_PWD, example = E_PWD, required = true)
    @NotNull(message = M_PWD_NOT_NULL)
    @Length(max = 16, message = M_PWD_MAX)
    private String pwd;

    @ApiModelProperty(notes = N_CARD, example = E_CARD)
    @Length(max = 1, message = M_CARD_MAX)
    private String card;

    @ApiModelProperty(notes = N_TEL, example = E_TEL, required = true)
    @NotNull(message = M_TEL_NOT_NULL)
    @Length(max = 11, message = M_TEL_MAX)
    private String tel;


}

