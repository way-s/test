package org.jsola.host.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.jsola.core.Page;
import org.jsola.core.Result;
import org.jsola.host.dto.BookStoreAddDTO;
import org.jsola.host.dto.BookStoreUpdateDTO;
import org.jsola.host.query.BookStoreQuery;
import org.jsola.host.vo.BookStoreListVO;
import org.jsola.host.vo.BookStoreVO;

import java.util.List;

/**
 * 书籍信息表
 *
 * @author huangrun
 */
@Api(tags = "jsola-书籍信息表")
public interface IBookStoreControllerAPI {

    /**
     * 新增书籍信息表
     * @param bookStoreAddDTO 书籍信息表新增实体
     * @return 书籍信息表展示对象
     */
    @ApiOperation(value = "新增书籍信息表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "bookStoreAddDTO", dataType = "BookStoreAddDTO", value = "新增书籍信息表", paramType = "body")
    })
    Result<BookStoreVO> save(BookStoreAddDTO bookStoreAddDTO);


    /**
     * 修改书籍信息表
     * @param bookStoreUpdateDTO 书籍信息表
     * @return 更新数量
     */
    @ApiOperation(value = "修改书籍信息表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "bookStoreUpdateDTO", dataType = "BookStoreUpdateDTO", value = "修改书籍信息表", paramType = "body")
    })
    Result<Integer> update(BookStoreUpdateDTO bookStoreUpdateDTO);




    /**
     * 删除单个书籍信息表，彻底删除
     * @param bookStoreId 书籍信息表id
     * @return 删除数量
     */
    @ApiOperation(value="删除单个书籍信息表，彻底删除")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "bookStoreId", dataType = "Long", value = "书籍信息表id", paramType = "path")
    })
    Result<Integer> delete(Long bookStoreId);


    /**
     * 批量删除书籍信息表，彻底删除
     * @param bookStoreIds 书籍信息表id集合
     * @return 删除数量
     */
    @ApiOperation(value = "批量删除书籍信息表，彻底删除")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "bookStoreIds", dataType = "Long[]", value = "书籍信息表id数组", paramType = "body" ,allowMultiple = true)
    })
    Result<Integer> deleteBatch(Long[] bookStoreIds);


    /**
     * 根据书籍信息表id查询书籍信息表详情
     * @param bookStoreId 书籍信息表id
     * @return 书籍信息表详情
     */
    @ApiOperation(value="根据书籍信息表id查询书籍信息表详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "bookStoreId", dataType = "Long", value = "书籍信息表id", paramType = "path")
    })
    Result<BookStoreVO> findById(Long bookStoreId);


    /**
     * 查询书籍信息表
     * @param bookStoreQuery 书籍信息表查询参数
     * @return page
     */
    @ApiOperation(value="查询书籍信息表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "bookStoreQuery", dataType = "BookStoreQuery", value = "书籍信息表查询参数", paramType = "query")
    })
    Result<List<BookStoreListVO>> listAll(BookStoreQuery bookStoreQuery);


     /**
     * 分页查询书籍信息表
     * @param bookStoreQuery 书籍信息表查询参数
     * @return page
     */
    @ApiOperation(value="分页查询书籍信息表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "bookStoreQuery", dataType = "BookStoreQuery", value = "书籍信息表查询参数", paramType = "query")
    })
    Result<Page<BookStoreListVO>> page(BookStoreQuery bookStoreQuery);


}

