package org.jsola.host.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.jsola.core.Page;
import org.jsola.core.Result;
import org.jsola.host.dto.AdminAddDTO;
import org.jsola.host.dto.AdminUpdateDTO;
import org.jsola.host.query.AdminQuery;
import org.jsola.host.vo.AdminListVO;
import org.jsola.host.vo.AdminVO;

import java.util.List;

/**
 * 管理员
 *
 * @author huangrun
 */
@Api(tags = "jsola-管理员")
public interface IAdminControllerAPI {

    /**
     * 新增管理员
     * @param adminAddDTO 管理员新增实体
     * @return 管理员展示对象
     */
    @ApiOperation(value = "新增管理员")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "adminAddDTO", dataType = "AdminAddDTO", value = "新增管理员", paramType = "body")
    })
    Result<AdminVO> save(AdminAddDTO adminAddDTO);


    /**
     * 修改管理员
     * @param adminUpdateDTO 管理员
     * @return 更新数量
     */
    @ApiOperation(value = "修改管理员")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "adminUpdateDTO", dataType = "AdminUpdateDTO", value = "修改管理员", paramType = "body")
    })
    Result<Integer> update(AdminUpdateDTO adminUpdateDTO);




    /**
     * 删除单个管理员，彻底删除
     * @param adminId 管理员id
     * @return 删除数量
     */
    @ApiOperation(value="删除单个管理员，彻底删除")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "adminId", dataType = "Long", value = "管理员id", paramType = "path")
    })
    Result<Integer> delete(Long adminId);


    /**
     * 批量删除管理员，彻底删除
     * @param adminIds 管理员id集合
     * @return 删除数量
     */
    @ApiOperation(value = "批量删除管理员，彻底删除")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "adminIds", dataType = "Long[]", value = "管理员id数组", paramType = "body" ,allowMultiple = true)
    })
    Result<Integer> deleteBatch(Long[] adminIds);


    /**
     * 根据管理员id查询管理员详情
     * @param adminId 管理员id
     * @return 管理员详情
     */
    @ApiOperation(value="根据管理员id查询管理员详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "adminId", dataType = "Long", value = "管理员id", paramType = "path")
    })
    Result<AdminVO> findById(Long adminId);


    /**
     * 查询管理员
     * @param adminQuery 管理员查询参数
     * @return page
     */
    @ApiOperation(value="查询管理员")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "adminQuery", dataType = "AdminQuery", value = "管理员查询参数", paramType = "query")
    })
    Result<List<AdminListVO>> listAll(AdminQuery adminQuery);


     /**
     * 分页查询管理员
     * @param adminQuery 管理员查询参数
     * @return page
     */
    @ApiOperation(value="分页查询管理员")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "adminQuery", dataType = "AdminQuery", value = "管理员查询参数", paramType = "query")
    })
    Result<Page<AdminListVO>> page(AdminQuery adminQuery);


}

