package org.jsola.host.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.jsola.core.Page;
import org.jsola.core.Result;
import org.jsola.host.dto.StudentAddDTO;
import org.jsola.host.dto.StudentUpdateDTO;
import org.jsola.host.query.StudentQuery;
import org.jsola.host.vo.StudentListVO;
import org.jsola.host.vo.StudentVO;

import java.util.List;

/**
 * 学生表
 *
 * @author huangrun
 */
@Api(tags = "jsola-学生表")
public interface IStudentControllerAPI {

    /**
     * 新增学生表
     * @param studentAddDTO 学生表新增实体
     * @return 学生表展示对象
     */
    @ApiOperation(value = "新增学生表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "studentAddDTO", dataType = "StudentAddDTO", value = "新增学生表", paramType = "body")
    })
    Result<StudentVO> save(StudentAddDTO studentAddDTO);


    /**
     * 修改学生表
     * @param studentUpdateDTO 学生表
     * @return 更新数量
     */
    @ApiOperation(value = "修改学生表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "studentUpdateDTO", dataType = "StudentUpdateDTO", value = "修改学生表", paramType = "body")
    })
    Result<Integer> update(StudentUpdateDTO studentUpdateDTO);




    /**
     * 删除单个学生表，彻底删除
     * @param studentId 学生表id
     * @return 删除数量
     */
    @ApiOperation(value="删除单个学生表，彻底删除")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "studentId", dataType = "Long", value = "学生表id", paramType = "path")
    })
    Result<Integer> delete(Long studentId);


    /**
     * 批量删除学生表，彻底删除
     * @param studentIds 学生表id集合
     * @return 删除数量
     */
    @ApiOperation(value = "批量删除学生表，彻底删除")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "studentIds", dataType = "Long[]", value = "学生表id数组", paramType = "body" ,allowMultiple = true)
    })
    Result<Integer> deleteBatch(Long[] studentIds);


    /**
     * 根据学生表id查询学生表详情
     * @param studentId 学生表id
     * @return 学生表详情
     */
    @ApiOperation(value="根据学生表id查询学生表详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "studentId", dataType = "Long", value = "学生表id", paramType = "path")
    })
    Result<StudentVO> findById(Long studentId);


    /**
     * 查询学生表
     * @param studentQuery 学生表查询参数
     * @return page
     */
    @ApiOperation(value="查询学生表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "studentQuery", dataType = "StudentQuery", value = "学生表查询参数", paramType = "query")
    })
    Result<List<StudentListVO>> listAll(StudentQuery studentQuery);


     /**
     * 分页查询学生表
     * @param studentQuery 学生表查询参数
     * @return page
     */
    @ApiOperation(value="分页查询学生表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "studentQuery", dataType = "StudentQuery", value = "学生表查询参数", paramType = "query")
    })
    Result<Page<StudentListVO>> page(StudentQuery studentQuery);


}

