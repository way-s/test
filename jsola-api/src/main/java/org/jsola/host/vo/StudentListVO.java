package org.jsola.host.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.jsola.core.vo.BaseVO;

import static org.jsola.core.example.BaseExample.E_ID;
import static org.jsola.core.example.BaseExample.N_ID;
import static org.jsola.host.example.StudentExample.E_BRR_COUNT;
import static org.jsola.host.example.StudentExample.E_CARD;
import static org.jsola.host.example.StudentExample.E_NAME;
import static org.jsola.host.example.StudentExample.E_PWD;
import static org.jsola.host.example.StudentExample.E_TEL;
import static org.jsola.host.example.StudentExample.E_TIME_OUT;
import static org.jsola.host.example.StudentExample.N_BRR_COUNT;
import static org.jsola.host.example.StudentExample.N_CARD;
import static org.jsola.host.example.StudentExample.N_NAME;
import static org.jsola.host.example.StudentExample.N_PWD;
import static org.jsola.host.example.StudentExample.N_TEL;
import static org.jsola.host.example.StudentExample.N_TIME_OUT;

/**
 * 学生表
 *
 * @author huangrun
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "【学生表】列表展示对象")
@Data
public class StudentListVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(notes = N_ID, example = E_ID)
    private Long id;

    @ApiModelProperty(notes = N_TIME_OUT, example = E_TIME_OUT)
    private Integer timeOut;

    @ApiModelProperty(notes = N_BRR_COUNT, example = E_BRR_COUNT)
    private Integer brrCount;

    @ApiModelProperty(notes = N_NAME, example = E_NAME)
    private String name;

    @ApiModelProperty(notes = N_PWD, example = E_PWD)
    private String pwd;

    @ApiModelProperty(notes = N_CARD, example = E_CARD)
    private String card;

    @ApiModelProperty(notes = N_TEL, example = E_TEL)
    private String tel;



}

