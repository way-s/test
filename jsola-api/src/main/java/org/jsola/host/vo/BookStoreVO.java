package org.jsola.host.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.jsola.core.vo.BaseVO;

import java.math.BigDecimal;

import static org.jsola.core.example.BaseExample.E_ID;
import static org.jsola.core.example.BaseExample.N_ID;
import static org.jsola.host.example.BookStoreExample.E_AUTHOR;
import static org.jsola.host.example.BookStoreExample.E_BOOKMAN;
import static org.jsola.host.example.BookStoreExample.E_BOOK_NAME;
import static org.jsola.host.example.BookStoreExample.E_DOUBAN_SCORE;
import static org.jsola.host.example.BookStoreExample.E_TRANSLATOR;
import static org.jsola.host.example.BookStoreExample.N_AUTHOR;
import static org.jsola.host.example.BookStoreExample.N_BOOKMAN;
import static org.jsola.host.example.BookStoreExample.N_BOOK_NAME;
import static org.jsola.host.example.BookStoreExample.N_DOUBAN_SCORE;
import static org.jsola.host.example.BookStoreExample.N_TRANSLATOR;

/**
 * 书籍信息表
 *
 * @author huangrun
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "【书籍信息表】详情展示对象")
@Data
public class BookStoreVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(notes = N_ID, example = E_ID)
    private Long id;

    @ApiModelProperty(notes = N_BOOK_NAME, example = E_BOOK_NAME)
    private String bookName;

    @ApiModelProperty(notes = N_AUTHOR, example = E_AUTHOR)
    private String author;

    @ApiModelProperty(notes = N_TRANSLATOR, example = E_TRANSLATOR)
    private String translator;

    @ApiModelProperty(notes = N_BOOKMAN, example = E_BOOKMAN)
    private String bookman;

    @ApiModelProperty(notes = N_DOUBAN_SCORE, example = E_DOUBAN_SCORE)
    private BigDecimal doubanScore;



}

