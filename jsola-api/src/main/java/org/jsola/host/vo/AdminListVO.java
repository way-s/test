package org.jsola.host.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.jsola.core.vo.BaseVO;

import static org.jsola.core.example.BaseExample.E_ID;
import static org.jsola.core.example.BaseExample.N_ID;
import static org.jsola.host.example.AdminExample.E_NAME;
import static org.jsola.host.example.AdminExample.E_PWD;
import static org.jsola.host.example.AdminExample.E_TEL;
import static org.jsola.host.example.AdminExample.N_NAME;
import static org.jsola.host.example.AdminExample.N_PWD;
import static org.jsola.host.example.AdminExample.N_TEL;

/**
 * 管理员
 *
 * @author huangrun
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "【管理员】列表展示对象")
@Data
public class AdminListVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(notes = N_ID, example = E_ID)
    private Long id;

    @ApiModelProperty(notes = N_NAME, example = E_NAME)
    private String name;

    @ApiModelProperty(notes = N_PWD, example = E_PWD)
    private String pwd;

    @ApiModelProperty(notes = N_TEL, example = E_TEL)
    private String tel;



}

