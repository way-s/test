package org.jsola.host.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.jsola.core.entity.BaseDO;

import java.math.BigDecimal;
import javax.persistence.Table;

/**
 * 书籍信息表
 *
 * @author huangrun
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Table(name = "com_book_store")
public class BookStoreDO extends BaseDO<Long> {

    private static final long serialVersionUID = 1L;

    /**
     * 书名
     */
    private String bookName;
    /**
     * 作者
     */
    private String author;
    /**
     * 翻译
     */
    private String translator;
    /**
     * 发行社
     */
    private String bookman;
    /**
     * 发行时间
     */
    private BigDecimal doubanScore;



}

