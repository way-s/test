package org.jsola.host.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.jsola.core.entity.BaseDO;

import javax.persistence.Table;

/**
 * 管理员
 *
 * @author huangrun
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Table(name = "com_admin")
public class AdminDO extends BaseDO<Long> {

    private static final long serialVersionUID = 1L;

    /**
     * 名字
     */
    private String name;
    /**
     * 密码
     */
    private String pwd;
    /**
     * 编号
     */
    private String tel;



}

