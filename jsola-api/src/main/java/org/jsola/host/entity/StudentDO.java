package org.jsola.host.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.jsola.core.entity.BaseDO;

import javax.persistence.Table;

/**
 * 学生表
 *
 * @author huangrun
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Table(name = "com_student")
public class StudentDO extends BaseDO<Long> {

    private static final long serialVersionUID = 1L;

    /**
     * 超时
     */
    private Integer timeOut;
    /**
     * 计数
     */
    private Integer brrCount;
    /**
     * 名字
     */
    private String name;
    /**
     * 密码
     */
    private String pwd;
    /**
     * 卡 1 有,0 没有
     */
    private String card;
    /**
     * 学号
     */
    private String tel;



}

